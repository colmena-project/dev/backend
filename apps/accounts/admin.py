"""
Admin package for the accounts app.
"""

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import gettext_lazy
from django.db import models

from apps.accounts.form import UserInvitationCreatedForm
from apps.accounts.models import User, Language, UserInvitation
from colmena.admin import generate_preview


@admin.register(User)
class UserAdmin(UserAdmin):
    """
    Convenience class to define the UserAdmin admin view.
    """

    list_display = ("id", "username", "email", "full_name", "is_staff")
    list_filter = ("created_at",)
    search_fields = (
        "email",
        "full_name",
        "username",
        "email",
    )
    readonly_fields = ("avatar_preview",)

    add_fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": ("username", "password1", "password2"),
            },
        ),
        (
            gettext_lazy("users_admin_personal_info"),
            {
                "classes": ("wide",),
                "fields": ("full_name", "email", "language", "avatar"),
            },
        ),
        (
            gettext_lazy("users_admin_user_permissions"),
            {
                "classes": ("wide",),
                "fields": (
                    "is_active",
                    "is_staff",
                    "is_superuser",
                    "groups",
                    "user_permissions",
                ),
            },
        ),
        (
            gettext_lazy("users_prop_nc_app_password"),
            {"classes": ("wide",), "fields": ("nc_app_password",)},
        ),
    )
    fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": (
                    "username",
                    "password",
                ),
            },
        ),
        (
            gettext_lazy("users_admin_personal_info"),
            {
                "classes": ("wide",),
                "fields": (
                    "full_name",
                    "email",
                    "language",
                    "avatar_preview",
                    "avatar",
                ),
            },
        ),
        (
            gettext_lazy("users_admin_user_permissions"),
            {
                "classes": ("wide",),
                "fields": (
                    "is_active",
                    "is_staff",
                    "is_superuser",
                    "groups",
                    "user_permissions",
                ),
            },
        ),
        (
            gettext_lazy("users_prop_nc_app_password"),
            {"classes": ("wide",), "fields": ("nc_app_password",)},
        ),
        (gettext_lazy("last_login"), {"classes": ("",), "fields": ("last_login",)}),
    )

    def avatar_preview(self, obj):
        return generate_preview(obj.avatar)

    avatar_preview.short_description = gettext_lazy("Avatar Preview")
    avatar_preview.allow_tags = True

    def delete_queryset(self, request, queryset):
        # Overwrite this method to ensure that the model delete is used.
        for obj in queryset:
            obj.delete()

    def save_model(self, request, obj, form, change):
        non_m2m_fields = [
            field
            for field in form.changed_data
            if not isinstance(obj._meta.get_field(field), models.ManyToManyField)
        ]

        if non_m2m_fields:
            obj.save(update_fields=non_m2m_fields)
        else:
            obj.save()


@admin.register(Language)
class LanguageAdmin(admin.ModelAdmin):
    """
    Convenience class to define the LanguageAdmin admin view.
    """

    list_display = (
        "name",
        "iso_code",
    )


@admin.register(UserInvitation)
class UserInvitationAdmin(admin.ModelAdmin):
    """
    Convenience class to define the UserInvitationAdmin admin view.
    """

    actions_on_bottom = True
    actions_on_top = False
    exclude = (
        "password",
        "is_superuser",
        "last_login",
        "is_active",
        "is_staff",
        "user_permissions",
    )
    list_display = ("username", "email")
    search_fields = (
        "username",
        "email",
    )
    fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": (
                    "groups",
                    "username",
                ),
            },
        ),
        (
            None,
            {
                "classes": ("wide",),
                "fields": ("full_name", "email", "language", "region"),
            },
        ),
    )
    add_fieldsets = (
        (
            {
                "fields": (
                    "groups",
                    "username",
                ),
            },
        ),
        (
            {
                "classes": ("wide",),
                "fields": ("full_name", "email", "language"),
            },
        ),
    )

    def get_form(self, request, obj=None, **kwargs):
        # Show a OrganizationForm custom in creation
        if not obj:
            form = UserInvitationCreatedForm
            form.user = request.user
            return form
        return super(UserInvitationAdmin, self).get_form(request, obj, **kwargs)

    class Media:
        js = ("js/toggleRegionField.js",)
