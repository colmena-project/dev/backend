from django.apps import AppConfig
from django.utils.translation import gettext_lazy


class AccountsConfig(AppConfig):
    name = "apps.accounts"
    verbose_name = gettext_lazy("app_name_accounts")

    def ready(self):
        from . import receivers
