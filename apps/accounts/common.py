from apps.accounts.models import User


def is_superadmin_or_staff(user: User) -> bool:
    return is_superadmin(user) or user.belongs_to_group("Staff")


def is_superadmin(user: User) -> bool:
    return user.belongs_to_group("Superadmin")
