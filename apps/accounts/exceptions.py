from django.core.exceptions import ValidationError


class AccountException(Exception):
    """
    Raised when an account related exception is found.

    Attributes:
        message -- explanation of the error
    """

    def __init__(self, message=None):
        if message:
            self.message = message
        super().__init__(self.message)


class AccountDoesNotExistException(AccountException):
    """
    Raised when a `User` instance does not exist.
    """

    message: str = "The User could not be found."


class InvalidPasswordException(ValidationError):
    status_code = 400
    reason = "The password is not valid."

    def __init__(self):
        """Configure exception."""
        super(ValidationError, self).__init__()
