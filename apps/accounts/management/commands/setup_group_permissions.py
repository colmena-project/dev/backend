from apps.accounts import accounts

from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = "Assigns permissions to groups."

    def handle(self, *args, **options) -> None:
        accounts.configure_groups()
        self.stdout.write(
            msg=self.style.SUCCESS("Successfully ran groups configuration")
        )
        return None
