# Generated by Django 4.2.3 on 2023-07-19 22:24

import django.contrib.auth.models
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("accounts", "0002_language_remove_user_first_name_and_more"),
    ]

    operations = [
        migrations.CreateModel(
            name="UserInvitation",
            fields=[],
            options={
                "verbose_name": "User invitation",
                "verbose_name_plural": "User invitations",
                "proxy": True,
                "indexes": [],
                "constraints": [],
            },
            bases=("accounts.user",),
            managers=[
                ("objects", django.contrib.auth.models.UserManager()),
            ],
        ),
        migrations.AlterField(
            model_name="user",
            name="is_active",
            field=models.BooleanField(
                default=False,
                help_text="Designates whether this user should be treated as active. Unselect this instead of deleting users.",
                verbose_name="users_prop_is_active",
            ),
        ),
        migrations.AlterField(
            model_name="user",
            name="language",
            field=models.ManyToManyField(
                to="accounts.language", verbose_name="languages_model_name"
            ),
        ),
    ]
