import logging

from django.db.models.signals import pre_save, pre_delete, post_save
from django.dispatch import receiver
from django.core.exceptions import ValidationError

from apps.accounts.models import UserInvitation, User, get_super_admin
from apps.mails.exceptions import EmailDeliveryException
from apps.mails.invitations import send_invitation
from apps.invitations import invitations
from apps.organizations.organizations import get_personal_workspace
from apps.nextcloud import users as nextcloud_users


logger = logging.getLogger(__name__)

MODEL_FULL_NAME_FIELD = "full_name"
GROUP_ORG_OWNER_NAME = "OrgOwner"


@receiver(post_save, sender=UserInvitation)
def send_email_administration_invitation_confirmation(
    instance, created, **kwargs
) -> None:
    try:
        if created:
            invitations.create_administration_invitation(
                instance, instance._invited_by, instance._group, instance._language
            )
        else:
            invitation = invitations.get_administration_invitation(
                {"user__id": instance.id}
            )
            if instance.is_active and invitation.can_resend():
                invitation.mark_as_accepted()
                send_invitation(invitation)
    except Exception as e:
        logger.error(
            "Unexpected error while sending an email to a new staff user. Exception=%s",
            e,
            extra={
                "user_id": instance.id,
            },
        )
        raise EmailDeliveryException from e


@receiver(pre_delete, sender=User)
def remove_user_data_in_nextcloud(sender, instance, **kwargs) -> None:
    logger.debug(
        "Deleting user on nextcloud",
        extra={"sender": sender, "user_id": instance.id},
    )
    if not instance.belongs_to_group(GROUP_ORG_OWNER_NAME):
        _handle_pre_delete(instance)
    else:
        logger.error(
            "Error when trying to delete a user. The user is the owner of an organization.",
            extra={
                "user_id": instance.id,
                "nextcloud_user_id": instance.get_nextcloud_user_id(),
            },
        )
        raise ValidationError("ERRORS_NEXTCLOUD_DELETE_ORG_OWNER_USER")


def _handle_pre_delete(instance: User):
    try:
        superadmin = get_super_admin()
        nextcloud_user_id = instance.get_nextcloud_user_id()
        get_personal_workspace(nextcloud_user_id).delete()
        nextcloud_users.delete_user(
            nextcloud_user_id,
            superadmin.get_nextcloud_user_id(),
            superadmin.nc_app_password,
        )
    except Exception as e:
        logger.error(
            "Failed deleting a user",
            extra={
                "user_id": instance.id,
                "auth_user_id": superadmin.get_nextcloud_user_id(),
            },
        )
        raise ValidationError("ERRORS_NEXTCLOUD_DELETE_USER") from e


@receiver(pre_save, sender=User)
def update_nextcloud_display_name(sender, instance, **kwargs):
    logger.debug(
        "Updating the display name on nextcloud",
        extra={"sender": sender, "user_id": instance.id},
    )
    updated_fields = kwargs.get("update_fields")
    if instance.nc_app_password and _is_full_name_modified(updated_fields):
        _handle_pre_save(instance)


def _handle_pre_save(user: User):
    nextcloud_user_id = user.get_nextcloud_user_id()
    nextcloud_users.update_display_name(
        nextcloud_user_id,
        user.full_name,
        nextcloud_user_id,
        user.nc_app_password,
    )


def _is_full_name_modified(updated_fields) -> bool:
    return updated_fields and MODEL_FULL_NAME_FIELD in updated_fields
