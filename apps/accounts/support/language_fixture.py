import random
from apps.accounts.models import Language


VALID_ATTRS = {"name": "some name", "iso_code": "en"}

UPDATE_ATTRS = {"name": "some updated name", "iso_code": "es"}

INVALID_ATTRS = {
    "name": None,
    "iso_code": None,
}


def valid_attrs(attrs={}):
    return (
        VALID_ATTRS
        | {
            "name": unique_value(VALID_ATTRS["name"]),
            "iso_code": unique_value(VALID_ATTRS["iso_code"]),
        }
        | attrs
    )


def update_attrs(attrs={}):
    return (
        UPDATE_ATTRS
        | {
            "name": unique_value(UPDATE_ATTRS["name"]),
            "iso_code": unique_value(VALID_ATTRS["iso_code"]),
        }
        | attrs
    )


def invalid_attrs(attrs={}):
    return INVALID_ATTRS | attrs


def create(attrs={}):
    return Language.objects.create(**valid_attrs(attrs))


def unique_value(value):
    return f"{str(random.randrange(1,100))}-{value}"
