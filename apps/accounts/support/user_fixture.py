import random
from apps.accounts.models import User
from django.core.management import call_command
from colmena.settings.base import (
    SUPERADMIN_EMAIL,
    SUPERADMIN_PASSWORD,
    NEXTCLOUD_ADMIN_PASSWORD,
    NEXTCLOUD_ADMIN_USER,
)

VALID_ATTRS = {
    "email": "some@email.net",
    "username": "some-username",
    "password": "Ad_12da12:?1d",
    "full_name": "some full name",
    "is_superuser": True,
    "is_staff": True,
    "is_active": True,
}

UPDATE_ATTRS = {
    "email": "some_updated@email.net",
    "username": "some updated username",
    "password": "some updated password",
    "full_name": "some updated full name",
    "is_superuser": False,
    "is_staff": False,
    "is_active": False,
}

INVALID_ATTRS = {
    "email": None,
    "username": None,
    "password": None,
    "full_name": None,
    "is_superuser": None,
    "is_staff": None,
    "is_active": None,
}


def valid_attrs(attrs={}):
    return (
        VALID_ATTRS
        | {
            "username": unique_value(VALID_ATTRS["username"]),
            "email": unique_value(VALID_ATTRS["email"]),
        }
        | attrs
    )


def update_attrs(attrs={}):
    return (
        UPDATE_ATTRS
        | {
            "username": unique_value(UPDATE_ATTRS["username"]),
            "email": unique_value(UPDATE_ATTRS["email"]),
        }
        | attrs
    )


def invalid_attrs(attrs={}):
    return INVALID_ATTRS | attrs


def create(attrs={}):
    return User.objects.create(**valid_attrs(attrs))


def unique_value(value):
    return f"{str(random.randrange(1,1000000))}-{value}"


def create_superadmin():
    return call_command(
        "create_superadmin",
        SUPERADMIN_EMAIL,
        SUPERADMIN_PASSWORD,
        NEXTCLOUD_ADMIN_USER,
        NEXTCLOUD_ADMIN_PASSWORD,
    )
