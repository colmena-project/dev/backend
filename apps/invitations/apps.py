from django.apps import AppConfig
from django.utils.translation import gettext_lazy


class InvitationsConfig(AppConfig):
    name = "apps.invitations"
    verbose_name = gettext_lazy("app_name_invitations")
    default_auto_field = "django.db.models.BigAutoField"

    def ready(self):
        from . import receivers
