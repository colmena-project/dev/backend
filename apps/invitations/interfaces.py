from django.db import models
from abc import abstractmethod, ABCMeta


class SendableModelMeta(ABCMeta, type(models.Model)):
    pass


class SendableModel(models.Model, metaclass=SendableModelMeta):
    @abstractmethod
    def get_template_name(self) -> str:
        pass

    @abstractmethod
    def get_email_context(self) -> dict:
        pass

    @abstractmethod
    def get_invitation_link(self) -> str:
        pass

    @abstractmethod
    def get_language(self):
        pass

    @abstractmethod
    def validate(self):
        pass

    class Meta:
        abstract = True
