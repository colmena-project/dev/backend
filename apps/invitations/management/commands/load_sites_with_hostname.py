"""
This module defines an implementation to load sites with a given hostname.
"""
import json
import logging
import os

from django.core.management import call_command
from django.core.management.base import BaseCommand

logger: logging.Logger = logging.getLogger(name=__name__)


class Command(BaseCommand):
    """
    Updates sites according to the env vars set in the system environment.
    """

    def add_arguments(self, parser) -> None:
        parser.add_argument("backend_hostname", type=str)
        parser.add_argument("frontend_hostname", type=str)

    def handle(self, *args, **options) -> None:
        target_file = "apps/sites/seeds/00-sites.json"

        backend_host = options["backend_hostname"]
        frontend_host = options["frontend_hostname"]
        settings_module = options["settings"] or "colmena.settings.dev"
        logger.info("Using settings module `%s`", settings_module)

        try:
            logger.debug("Opening %s file to update sites hostnames...", target_file)
            with open(file=target_file, mode="r", encoding="utf-8") as f:
                seed_data = json.load(f)
        except FileNotFoundError:
            self.stderr.write(msg=f"File {target_file} not found")
            return

        for item in seed_data:
            domain = item["fields"]["domain"]
            if "<backend_host>" in domain:
                domain = item["fields"]["domain"].replace(
                    "<backend_host>", backend_host
                )

            if "<frontend_host>" in domain:
                domain = item["fields"]["domain"].replace(
                    "<frontend_host>", frontend_host
                )

            item["fields"]["domain"] = domain

        try:
            target_file_dirname: str = os.path.dirname(target_file)
            tmp_filename: str = os.path.join(target_file_dirname, "seed.tmp.json")
            with open(file=tmp_filename, mode="w", encoding="utf-8") as f:
                json.dump(seed_data, f, indent=2)

            logger.info(msg="Successfully updated Sites seed file with hostnames")

            logger.debug(msg="Installing the new fixture in the database...")

            call_command("loaddata", tmp_filename, settings=settings_module)

        except Exception as e:
            self.stderr.write(msg=f"An error occurred while writing to file: {e}")

        finally:
            os.remove(path=tmp_filename)
            logger.debug(msg="Cleaning up directory.")
