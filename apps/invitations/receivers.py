import logging

from .models import OrganizationInvitation, AdministrationInvitation, Invitation

from django.db.models.signals import post_save
from django.dispatch import receiver

from apps.mails import invitations as email_invitations
from apps.mails.exceptions import EmailDeliveryException


logger = logging.getLogger(__name__)


@receiver(post_save, sender=AdministrationInvitation)
def process_administration_invitation(
    sender, instance: AdministrationInvitation, created: bool, **kwargs
) -> None:
    _handle_post_save(sender, instance, created)


@receiver(post_save, sender=OrganizationInvitation)
def process_organization_invitation(
    sender, instance: OrganizationInvitation, created: bool, **kwargs
) -> None:
    _handle_post_save(sender, instance, created)


def _handle_post_save(sender, invitation: Invitation, created: bool) -> None:
    if created:
        logger.debug(
            "Processing an invitation creation",
            extra={"sender": sender, "invitation": invitation},
        )
        try:
            email_invitations.send_invitation(invitation=invitation)
            invitation.mark_as_sent()
        except EmailDeliveryException:
            invitation.mark_as_delivery_failed()

    else:
        logger.debug(
            "Processing an invitation update",
            extra={"sender": sender, "invitation": invitation},
        )
