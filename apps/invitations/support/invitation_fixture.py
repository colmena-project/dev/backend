from apps.invitations import invitations
from apps.invitations.models import OrganizationInvitation, InvitationStatus
from apps.accounts.support import user_fixture
from apps.organizations.support import organization_fixture

from django.contrib.auth.models import Group

from colmena.support.test_utils import unique_value


VALID_ATTRS = {
    "state": InvitationStatus.PENDING,
}

UPDATE_ATTRS = {
    "state": InvitationStatus.SENT,
}

INVALID_ATTRS = {
    "state": None,
}


def valid_attrs(attrs={}):
    return VALID_ATTRS | attrs


def update_attrs(attrs={}):
    return UPDATE_ATTRS | attrs


def invalid_attrs(attrs={}):
    return INVALID_ATTRS | attrs


def create(attrs={}) -> OrganizationInvitation:
    attrs = maybe_assign_user(attrs)
    attrs = maybe_assign_invited_by(attrs)
    attrs = maybe_assign_group(attrs)
    attrs = maybe_assign_organization(attrs)
    attrs = maybe_assign_token(attrs)
    return OrganizationInvitation.objects.create(**valid_attrs(attrs))


def maybe_assign_user(attrs):
    key = "user"
    if attrs.get(key) is None:
        attrs[key] = user_fixture.create(attrs.get("user_attrs", {}))
    return attrs


def maybe_assign_invited_by(attrs):
    key = "invited_by"
    if attrs.get(key) is None:
        attrs[key] = user_fixture.create(attrs.get("invited_by_attrs", {}))
    return attrs


def maybe_assign_group(attrs):
    key = "group"
    if attrs.get(key) is None:
        attrs[key] = Group.objects.create(
            **attrs.get("group_attrs", {"name": unique_value("some name")})
        )
    return attrs


def maybe_assign_organization(attrs):
    key = "organization"
    if attrs.get(key) is None:
        attrs[key] = organization_fixture.create(attrs.get("organization_attrs", {}))
    return attrs


def maybe_assign_token(attrs):
    key = "token"
    if attrs.get(key) is None:
        attrs[key] = invitations.generate_token_by_user(attrs["user"])
    return attrs
