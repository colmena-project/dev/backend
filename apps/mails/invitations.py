import logging

from .client import MailingClient

from apps.invitations.models import Invitation
from apps.mails.exceptions import EmailDeliveryException
from apps.mails.settings import get_noreply_from


logger = logging.getLogger(__name__)


def email_from_invitation(
    invitation: Invitation,
):
    template_name: str = invitation.get_template_name()
    mailing_client = MailingClient()
    email_template: str = mailing_client.get_template_by_language(
        templated_email=template_name,
        iso_code=invitation.get_language(),
    )
    email = mailing_client.build(
        from_email=get_noreply_from(),
        to=[invitation.user.email],
        template=email_template,
        context=invitation.get_email_context(),
    )

    return email


def send_invitation(invitation: Invitation) -> None:
    try:
        email = email_from_invitation(invitation)
        email.send()
    except Exception as e:
        _log_unexpected_error(e, invitation, "invitation")
        raise EmailDeliveryException


def _log_unexpected_error(e, invitation, type):
    logger.error(
        "Unexpected error while sending %s. Exception=%s",
        type,
        e,
        extra={"invitation_id": invitation.id},
    )
