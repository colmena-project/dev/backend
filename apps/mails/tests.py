from django.test import TestCase, tag
from django.template.exceptions import TemplateDoesNotExist

from apps.mails.client import MailingClient


class MailsClientTest(TestCase):
    fixtures = [
        "apps/sites/seeds/00-sites.json",
    ]
    mailing_client = None

    def setUp(self):
        self.mailing_client = MailingClient()
        self.valid_email_args = valid_email_args()

    def test_build_welcome_message(self):
        # Setup
        valid_args = self.valid_email_args

        # Exercise
        email = self.mailing_client.build(
            valid_args["from_email"],
            valid_args["to"],
            valid_args["template_name"],
            valid_args["context"],
        )

        # Verify
        assert valid_args["to"][0] in email.recipients()
        assert valid_args["to"][1] in email.recipients()

        self.assertEqual(valid_args["from_email"], email.from_email)

        assert f"Hi {valid_args['context']['full_name']}," in email.body
        assert (
            f"<dt>username</dt><dd>{valid_args['context']['username']}</dd>"
            in email.body
        )
        assert (
            f"<dt>join date</dt><dd>{valid_args['context']['signup_date']}</dd>"
            in email.body
        )

    def test_build_welcome_message_fails_on_unknown_email_template(self):
        # Setup
        valid_args = self.valid_email_args

        # Exercise/Verify
        with self.assertRaises(TemplateDoesNotExist):
            self.mailing_client.build(
                valid_args["from_email"],
                valid_args["to"],
                "some_invalid_template_name",
                valid_args["context"],
            )

    def test_send_email_message(self):
        # Setup
        valid_args = self.valid_email_args
        email = self.mailing_client.build(
            valid_args["from_email"],
            valid_args["to"],
            valid_args["template_name"],
            valid_args["context"],
        )

        # Exercise
        result = email.send()

        # Verify
        self.assertTrue(result)

    def test_send_email_message_fails_on_empty_recipients(self):
        # Setup
        valid_args = self.valid_email_args
        email = self.mailing_client.build(
            valid_args["from_email"],
            [],
            valid_args["template_name"],
            valid_args["context"],
        )

        # Exercise
        result = email.send()

        # Verify
        self.assertFalse(result)


def valid_email_args(args={}):
    return {
        "from_email": "admin@colmena.media",
        "template_name": "welcome",
        "to": ["some_user@colmena.media", "another_user@colmena.media"],
        "context": {
            "username": "some user",
            "full_name": "some full name",
            "signup_date": "some date",
        },
    } | args
