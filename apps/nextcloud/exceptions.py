from nextcloud_async.exceptions import NextCloudException, NextCloudNotFound


class NextcloudFileNotFound(NextCloudException):
    status_code = 400
    reason = "File not found."

    def __init__(self):
        """Configure exception."""
        super(NextCloudException, self).__init__()


class NextcloudUserIsNotFileOwner(NextCloudException):
    status_code = 403
    reason = "The user does not own the file."

    def __init__(self):
        """Configure exception."""
        super(NextCloudException, self).__init__()


class NextcloudUserNotFound(NextCloudNotFound):
    status_code = 404
    reason = "User not found."

    def __init__(self):
        """Configure exception."""
        super(NextCloudException, self).__init__()


class NextcloudPasswordCreationException(NextCloudException):
    status_code = 400
    reason = "The app password could not be created."

    def __init__(self):
        """Configure exception."""
        super(NextCloudException, self).__init__()


class NextcloudGroupCreationException(NextCloudException):
    status_code = 400
    reason = "The group could not be created."

    def __init__(self):
        """Configure exception."""
        super(NextCloudException, self).__init__()


class NextcloudConversationCreationException(NextCloudException):
    status_code = 400
    reason = "The conversation could not be created."

    def __init__(self):
        """Configure exception."""
        super(NextCloudException, self).__init__()


class NextcloudAddUserToGroupException(NextCloudException):
    status_code = 400
    reason = "The user could be added to the group."

    def __init__(self):
        """Configure exception."""
        super(NextCloudException, self).__init__()


class NextcloudVeryCommonPasswordException(NextCloudException):
    status_code = 400
    reason = "The password is too common"

    def __init__(self):
        """Configure exception."""
        super(NextCloudException, self).__init__()


class NextcloudUnexpectedUserCreationException(NextCloudException):
    status_code = 400
    reason = "The user could not be created."

    def __init__(self):
        """Configure exception."""
        super(NextCloudException, self).__init__()


class NextcloudUploadFileException(NextCloudException):
    status_code = 400
    reason = "The project could not be uploaded."

    def __init__(self):
        """Configure exception."""
        super(NextCloudException, self).__init__()


class NextcloudGetProjectsException(NextCloudException):
    status_code = 400
    reason = "The projects could not be listed"

    def __init__(self):
        """Configure exception."""
        super(NextCloudException, self).__init__()


class NextcloudProjectNotFoundException(NextCloudException):
    """
    Riased when a file that represents a project could not be found.
    """

    status_code = 404
    reason = "The project could not be found"

    def __init__(self):
        """Configure exception."""
        super(NextCloudException, self).__init__()


class NextcloudDownloadFileException(NextCloudException):
    """
    Raised when the download process unexpectedly fails.
    """

    status_code = 400
    reason = "The file could not be downloaded"

    def __init__(self):
        """Configure exception."""
        super(NextCloudException, self).__init__()


class NextcloudUserConversationsFetchFailedException(NextCloudException):
    """
    Raised when the user conversations couldn't be fetched.
    """

    status_code = 400
    reason = "User conversations couldn't be retrieved"

    def __init__(self):
        """Configure exception."""
        super(NextCloudException, self).__init__()


class NextcloudRemoveUserFromConversationFailedException(NextCloudException):
    """
    Raised when the user could not be removed from the conversation.
    """

    status_code = 400
    error_code = "ERRORS_NEXTCLOUD_REMOVE_USER_FROM_CONVERSATION_FAILED"
    reason = "Could not remove user from conversation"

    def __init__(self):
        """Configure exception."""
        super(NextCloudException, self).__init__()


class NextcloudRemoveUserFromGroupFailedException(NextCloudException):
    """
    Raised when the user could not be removed from the nextcloud group.
    """

    status_code = 400
    error_code = "ERRORS_NEXTCLOUD_REMOVE_USER_FROM_GROUP_FAILED"
    reason = "Could not remove user from group"

    def __init__(self):
        """Configure exception."""
        super(NextCloudException, self).__init__()


class NextcloudLeaveConversationFailedException(NextCloudException):
    """
    Raised when the user could not leave the conversation.
    """

    status_code = 400
    error_code = "ERRORS_NEXTCLOUD_LEAVE_CONVERSATION_FAILED"
    reason = "Could not leave conversation"

    def __init__(self):
        """Configure exception."""
        super(NextCloudException, self).__init__()


class NextcloudDemoteUserFromAdminFailedException(NextCloudException):
    """
    Raised when the user cannot be demoted from the admin group.
    """

    status_code = 400
    error_code = "ERRORS_NEXTCLOUD_DEMOTE_USER_PERMISSIONS_FAILED"
    reason = "Could not demote user from the admin group"

    def __init__(self):
        """Configure exception."""
        super(NextCloudException, self).__init__()


class NextcloudDemoteUserFromSubadminFailedException(NextCloudException):
    """
    Raised when the user cannot be demoted from subadmin.
    """

    status_code = 400
    error_code = "ERRORS_NEXTCLOUD_DEMOTE_USER_PERMISSIONS_FAILED"
    reason = "Could not demote user from subadmin"

    def __init__(self):
        """Configure exception."""
        super(NextCloudException, self).__init__()
