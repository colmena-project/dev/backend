"""
This module is responsible for providing convienence functions to interact
with nextcloud files in a more complex way.
While the `resources.files` module provides a direct API to nextcloud files, this
module combines some of those APIs to perform more complex operations, such as
uploading and sharing a file in a single step, retrieving and filter lists
of shares or files, etc.
"""

import logging
import os

from django.core.exceptions import ValidationError
from colmena.serializers.serializers import ErrorSerializer
from apps.accounts.models import User
from apps.nextcloud.config import get_projects_dir, get_talk_dir
from apps.nextcloud.resources import utils as nextcloud_utils
from apps.nextcloud.resources import shares as nextcloud_shares_manager
from apps.nextcloud.resources import files as nextcloud_files_manager
from apps.nextcloud.exceptions import (
    NextcloudFileNotFound,
    NextcloudUserIsNotFileOwner,
    NextcloudProjectNotFoundException,
    NextcloudGetProjectsException,
)

logger = logging.getLogger(__name__)

PROJECTS_DIR = get_projects_dir()

TALK_DIR = get_talk_dir()


def send_as_attachment(new_file, filename, conversation_token, user):
    """
    Given a file, a filename, a conversation token and a user uploads and shares
    a file in the conversation associated to the given conversation token.

    Parameters
    ----------

    new_file : InMemoryUploadedFile
        A file in memory.

    filename : str
        A file name with extension.

    conversation_token : str
        The conversation token

    user : User
        A user model. This user is assumed to have the required
        privileges to perform nextcloud requests with an app password.

    Returns
    -------
    {
        nextcloud_upload_path : str
    }

    Raises
    ------
    ValidationError
        When the conversation_token is not a valid value.
    """
    try:
        upload_file = upload(new_file, filename, TALK_DIR, user)
        data = share(upload_file["nextcloud_upload_path"], conversation_token, user)
        return data
    except Exception as e:
        raise ValidationError(ErrorSerializer("ERRORS_SEND_ATTACHMENT").data) from e


def upload(file, filename, nextcloud_path, user):
    """
    Given a file, a filename and a user, uploads the file to the nextcloud instance.

    Parameters
    ----------

    file : InMemoryUploadedFile
        A file in memory.

    filename : str
        A file name with extension.

    nextcloud_path : str
        A valid path in nextcloud.

    user : User
        A user model. This user is assumed to have the required
        privileges to perform nextcloud requests with an app password.

    Returns
    -------
    {
        nextcloud_upload_path : String
    }

    Raises
    ------
    ValidationError
        When the conversation_token is not a valid value.
    """
    try:
        remote_path = _get_file_remote_path(nextcloud_path, filename)
        local_path = nextcloud_utils.handle_uploaded_file(file)
        nextcloud_files_manager.upload(
            local_path,
            filename,
            remote_path,
            user,
        )
        return {"nextcloud_upload_path": remote_path}
    except Exception as e:
        raise ValidationError(
            ErrorSerializer("ERRORS_NEXTCLOUD_UPLOAD_FILE").data
        ) from e
    finally:
        _remove_file(local_path)
        file.close()


def share(file_path, conversation_token, user):
    """
    Given a file_path, a conversation_token and a user to authenticate the request,
    sends a shared file in the conversation associated to the given conversation token.


    Parameters
    ----------

    file_path : str
        A valid path.

    conversation_token : str
        The conversation token.

    user : User
        A user model. This user is assumed to have the required
        privileges to perform nextcloud requests with an app password.

    Returns
    -------
    {
        share_id : Number,
        file_owner: str,
    }

    """
    try:
        data = nextcloud_shares_manager.create_conversation_share(
            file_path, conversation_token, user
        )
        return {"share_id": data["id"], "file_owner": data["displayname_file_owner"]}
    except Exception as e:
        raise ValidationError(
            ErrorSerializer("ERRORS_NEXTCLOUD_SHARE_ERROR").data
        ) from e


def download(file_id: int, user: User) -> bytes | bytearray | memoryview:
    """
    Given a file_id and a user to authenticate the request,
    download a file.


    Parameters
    ----------

    file_id : int
        A file id.

    user : User
        A user model. This user is assumed to have the required
        privileges to perform nextcloud requests with an app password.

    Returns
    -------
    ByteString

    """
    try:
        return _download(file_id, get_file, user)
    except Exception as e:
        raise ValidationError(
            ErrorSerializer("ERRORS_NEXTCLOUD_DOWNLOAD_ERROR").data
        ) from e


def download_project(file_id: int, user: User) -> bytes | bytearray | memoryview:
    """
    Given a `file_id` and a user to authenticate the request,
    download a file that represents a project.


    Parameters
    ----------

    file_id : int
        A file id.

    user : User
        A user model. This user is assumed to have the required
        privileges to perform nextcloud requests with an app password.

    Returns
    -------
    ByteString

    """
    return _download(file_id, get_project, user)


def _download(file_id, get_file_fn, user) -> bytes | bytearray | memoryview:
    file_path = get_nextcloud_file_path(get_file_fn(file_id, user), user)
    return nextcloud_files_manager.download_file(file_path, user)


def delete(file_id, user):
    """
    Given a file_id and a user to authenticate the request,
    deletes a file.

    Parameters
    ----------

    file_id : int
        A file id.

    user : User
        A user model. This user is assumed to have the required
        privileges to perform nextcloud requests with an app password.

    Returns
    -------
    None

    """
    try:
        file = get_file(file_id, user)
        _check_file_ownership(file, user)
        path = get_nextcloud_file_path(file, user)
        nextcloud_files_manager.delete_file(path, user)
    except NextcloudFileNotFound:
        raise
    except NextcloudUserIsNotFileOwner:
        raise
    except Exception as e:
        raise ValidationError(
            ErrorSerializer("ERRORS_NEXTCLOUD_FILE_DELETION_ERROR").data
        ) from e


def get_projects_by_user(user: User):
    """
    Given a user to authenticate the request, return all user projects.

    Parameters
    ----------

    user : User
        A user model. This user is assumed to have the required
        privileges to perform nextcloud requests with an app password.

    Returns
    -------
    [
        {
            "id": int,
            "name": str,
            "shared_teams": [
                {
                    "team_id": int,
                    "name": str
                },
            ]
        },
    ]

    """

    try:
        shares = nextcloud_shares_manager.get_all_shares(user)
        project_shares = [share for share in shares if _is_project(share)]
        return _filter_project_shares(project_shares)
    except NextcloudGetProjectsException:
        raise
    except Exception as e:
        logger.error(
            "Failed getting all projects for the user",
            extra={
                "auth_user_id": user.id,
            },
        )
        raise ValidationError(
            ErrorSerializer("ERRORS_NEXTCLOUD_GET_PROJECTS_ERROR").data
        ) from e


def get_file(file_id, user):
    """
    Given a file_id and a user, calls the Nextcloud API wrapper to get a single
    file.
    """
    try:
        files = nextcloud_files_manager.list_files(user=user)
        return _get_file(file_id, files)
    except Exception as e:
        raise ValidationError(
            ErrorSerializer("ERRORS_NEXTCLOUD_FILE_NOT_FOUND").data
        ) from e


def get_project(file_id: int, user: User):
    """
    Given a file_id and a user, calls the Nextcloud API wrapper to get a single
    file that represents a project.
    """
    try:
        projects = nextcloud_files_manager.list_projects(user=user)
        return _get_file(file_id, projects)
    except Exception as e:
        raise NextcloudProjectNotFoundException from e


def _get_file(file_id, files):
    try:
        file = nextcloud_utils.get_file_by_id(file_id, files)
        return file
    except NextcloudProjectNotFoundException:
        logger.error("The nextcloud file with file_id=%s could not be found.", file_id)
        raise


def get_nextcloud_file_path(file, user):
    """
    Given a Nextcloud file struct, returns it's file path.
    """
    try:
        return nextcloud_utils.get_relative_file_path(
            nextcloud_utils.get_file_path(file), user.get_nextcloud_user_id()
        )
    except Exception as e:
        raise ValidationError(
            ErrorSerializer("ERRORS_NEXTCLOUD_FILE_PATH_PARSE_ERROR").data
        ) from e


def _check_file_ownership(file: dict, user: User):
    if nextcloud_utils.get_file_owner_id(file) != user.get_nextcloud_user_id():
        raise NextcloudUserIsNotFileOwner


def _remove_file(local_path: str):
    if os.path.exists(local_path):
        os.remove(local_path)


def _get_file_remote_path(base_path: str, filename: str):
    return os.path.join(base_path, filename)


def _filter_project_shares(shares: dict):
    from itertools import groupby

    grouped_teams_by_shares = [
        list(shares_group)
        for file_id, shares_group in groupby(
            shares, key=nextcloud_shares_manager.get_file_id
        )
    ]
    return _flatten_teams_by_shares(grouped_teams_by_shares)


def _flatten_teams_by_shares(grouped_teams_by_shares: list):
    return [
        _flatten_teams_by_share(grouped_teams_by_share)
        for grouped_teams_by_share in grouped_teams_by_shares
    ]


def _flatten_teams_by_share(grouped_teams_by_share: list):
    """
    Given a list containing dict where an asset has been shared, returns a dict
    with team names flattened in a single list.

    Parameters
    ----------
    grouped_teams_by_share:

    [
        {
            'id':str,
            'share_type':int,
            'uid_owner':str,
            'displayname_owner':str,
            'permissions':int,
            'can_edit':bool,
            'can_delete':bool,
            'stime':int,
            'parent':dict,
            'expiration':str,
            'token':str,
            'uid_file_owner':str,
            'note':str,
            'label':str,
            'displayname_file_owner':str,
            'path':str,
            'item_type':str,
            'mimetype':str,
            'has_preview':bool,
            'storage_id':str,
            'storage':int,
            'item_source':int,
            'file_source':int,
            'file_parent':int,
            'file_target':str,
            'share_with':str,
            'share_with_displayname':str,
            'share_with_link':str,
            'mail_send':int,
            'hide_download':int,
            'attributes':dict
        },
    ]

    Returns
    -------
    {
        id : int,
        name: str,
        shared_teams: [str]
    }
    """
    project_response = lambda grouped_team_by_share: _build_project_info(
        grouped_team_by_share,
        [nextcloud_shares_manager.get_share_with_displayname(grouped_team_by_share)],
    )
    shared_teams = [
        team
        for grouped_team_by_share in grouped_teams_by_share
        for team in project_response(grouped_team_by_share)["shared_teams"]
    ]
    return _build_project_info(grouped_teams_by_share[0], shared_teams)


def _build_project_info(nc_share: dict, shared_teams: list):
    """
    Build a dictionary with information about a project and the shared teams.

    Parameters
    ----------

    nc_share:
    {
        'id':str,
        'share_type':int,
        'uid_owner':str,
        'displayname_owner':str,
        'permissions':int,
        'can_edit':bool,
        'can_delete':bool,
        'stime':int,
        'parent':dict,
        'expiration':str,
        'token':str,
        'uid_file_owner':str,
        'note':str,
        'label':str,
        'displayname_file_owner':str,
        'path':str,
        'item_type':str,
        'mimetype':str,
        'has_preview':bool,
        'storage_id':str,
        'storage':int,
        'item_source':int,
        'file_source':int,
        'file_parent':int,
        'file_target':str,
        'share_with':str,
        'share_with_displayname':str,
        'share_with_link':str,
        'mail_send':int,
        'hide_download':int,
        'attributes':dict
    }

    shared_team :[str]

    Returns
    -------

    {
        id : int,
        name: str,
        shared_teams: [str]
    }
    """
    return {
        "id": nextcloud_shares_manager.get_file_id(nc_share),
        "name": _get_file_name(nc_share),
        "shared_teams": shared_teams,
    }


def _get_file_name(nc_share: dict):
    path = nextcloud_shares_manager.get_path(nc_share)
    name = path.replace(PROJECTS_DIR, "")
    return name


def _is_project(nc_share: dict):
    return (
        PROJECTS_DIR in nc_share.get("path") and nc_share.get(("item_type")) == "file"
    )
