import logging

from .openapi import send_app_password_request, AppPasswordOut
from .openapi.client.api_config import HTTPException


logger = logging.getLogger(__name__)


def create_app_password(nc_username, nc_password):
    """
    Given a nextcloud username and a password requests an app password
    creation for the username provided and returns the generated value.

    If the request is successful the raw value is returned.

    Parameters
    ----------
    nc_username : str
        The username of a nextcloud user

    nc_password : str
        The username password

    Raises
    ------
    AppPasswordException
        If the connection can't be established or credentials are not valid.

    """
    try:
        response: AppPasswordOut = send_app_password_request(
            username=nc_username, password=nc_password
        )
        if response.app_password is not None:
            return response.app_password
        raise AppPasswordException(message="Unknown error while creating app password")

    except HTTPException as e:
        logger.error(
            "Failed creating app password",
            extra={"nc_username": nc_username, "nc_password": nc_password},
        )
        msg = e.message
        logger.error(
            "App password creation failed",
            extra={"nc_username": nc_username, "nc_password": nc_password, "msg": msg},
        )
        raise AppPasswordException(message=msg) from e


class AppPasswordException(Exception):
    """
    Raised when the app password could not be created.

    Attributes:
        message -- explanation of the error
    """

    def __init__(self, message="App password creation failed"):
        self.message = message
        super().__init__(self.message)
