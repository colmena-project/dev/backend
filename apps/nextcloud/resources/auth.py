import httpx
from django.core.exceptions import ValidationError
from nextcloud_async import NextCloudAsync
from colmena.settings.base import ASYNC_CLIENT_TIMEOUT
from apps.nextcloud.config import get_nextcloud_api_url

import logging

logger = logging.getLogger(__name__)


# RESULT: It seems that we can un cache pickling the object because is async
def auth(user_id, user_app_password):
    if not user_app_password:
        logger.error("App Password is blank", extra={"user_id": user_id})
        raise ValidationError("ERRORS_NEXTCLOUD_APP_PASSWORD_IS_BLANK")

    nca = NextCloudAsync(
        client=httpx.AsyncClient(timeout=ASYNC_CLIENT_TIMEOUT),
        endpoint=get_nextcloud_api_url(),
        user=user_id,
        password=user_app_password,
    )

    return nca
