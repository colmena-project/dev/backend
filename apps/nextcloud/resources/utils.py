import os
import re
import urllib
from colmena.serializers.serializers import ErrorSerializer
from django.core.exceptions import ValidationError
from django.conf import settings
from colmena.settings.base import SUPERADMIN_EMAIL

REMOTE_DAV_FILES_PATH = "/remote.php/dav/files/"


def validate_group_id(group_id):
    if not group_id:
        raise ValidationError(
            ErrorSerializer("ERRORS_NEXTCLOUD_GROUP_ID_IS_BLANK").data
        )


def validate_user_id(user_id):
    if not user_id:
        raise ValidationError(ErrorSerializer("ERRORS_NEXTCLOUD_USER_ID_IS_BLANK").data)


def validate_not_admin_group(group_id):
    if group_id == "admin":
        raise ValidationError(ErrorSerializer("ERRORS_NEXTCLOUD_GROUP_EXIST").data)


def validate_conversation_token(token):
    if not token:
        raise ValidationError(
            ErrorSerializer("ERRORS_NEXTCLOUD_CONVERSATION_ID_IS_BLANK").data
        )


def validate_message_id(message_id):
    if not message_id and not isinstance(las, str):
        raise ValidationError(
            ErrorSerializer("ERRORS_NEXTCLOUD_MESSAGE_ID_IS_INVALID").data
        )


def get_conversation_token(conversation=None):
    try:
        return conversation["token"]
    except nextcloud_exceptions.NextCloudException as e:
        raise ValidationError(ErrorSerializer("ERRORS_NEXTCLOUD_TOKEN").data)


def validate_filter_value(value=None):
    if not value:
        raise ValidationError(
            ErrorSerializer("ERRORS_NEXTCLOUD_FILTER_VALUE_IS_BLANK").data
        )


def validate_room_name(room_name=None):
    if not room_name:
        raise ValidationError(
            ErrorSerializer("ERRORS_NEXTCLOUD_ROOM_NAME_IS_BLANK").data
        )


def extract_organization_pk_to_group_id(group_id):
    substrings = string.split("-")
    return substrings[1]


def validate_user(user=None):
    pass


def handle_uploaded_file(f):
    abs_file = os.path.join(settings.MEDIA_ROOT, f.name)
    with open(abs_file, "wb+") as destination:
        for chunk in f.chunks():
            destination.write(chunk)
    return abs_file


def get_file_by_id(file_id, files):
    try:
        for _file in files:
            if get_file_id(_file) == file_id:
                return _file
        raise ValidationError("ERRORS_FILE_NOT_FOUND_ERROR")
    except Exception as e:
        raise ValidationError("ERRORS_GET_FILE_ERROR")


def get_file_id(file):
    try:
        return int(file["d:propstat"][0].get("d:prop").get("oc:fileid"))
    except Exception as e:
        raise ValidationError("ERRORS_PARSE_FILE_ID_ERROR")


def get_relative_file_path(file_path, username):
    remote_path = f"{REMOTE_DAV_FILES_PATH}{username}/"
    relative_path = file_path.replace(remote_path, "")
    return relative_path


def get_file_path(file):
    return file["d:href"]


def get_file_owner_id(file):
    return file["d:propstat"][0].get("d:prop").get("oc:owner-id")


def format_file_name(file_name: str) -> str:
    """
    Given a relative or full path for a file, returns the corresponding file name.
    """
    _, _, after = file_name.rpartition("/")
    if after:
        return after.strip()
    return file_name


def create_file_name(file_name: str, existing_file_names_list: list[str]) -> str:
    """
    Given a name for a file and a list of current files,
    determines whether the given name can be used
    or else returns a new name with a number of the copy assigned to it.
    """

    formatted_name = format_file_name(file_name)
    base_name, extension = os.path.splitext(formatted_name)
    pattern = re.compile(
        rf"^{re.escape(base_name)}(?:\s*\((\d+)\))?{re.escape(extension)}$"
    )

    matching_files = []
    max_index = 0

    for name in existing_file_names_list:
        f = urllib.parse.unquote(name)
        match = pattern.match(f)
        if match:
            matching_files.append(f)
            if match.group(1):
                max_index = max(max_index, int(match.group(1)))

    if matching_files:
        new_file_name = f"{base_name}({max_index + 1}){extension}"
    else:
        new_file_name = file_name

    return new_file_name
