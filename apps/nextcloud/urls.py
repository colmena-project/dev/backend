from . import views
from django.urls import path

urlpatterns = [
    path(
        "conversations",
        views.NextcloudConversationsAPIView.as_view(),
        name="list_conversations",
    ),
    path(
        "conversations/<conversation_token>/files",
        views.NextcloudConversationFilesAPIView.as_view(),
        name="conversation_files",
    ),
    path(
        "conversations/<conversation_token>",
        views.NextcloudConversationDetailAPIView.as_view(),
        name="get_conversation",
    ),
    path(
        "conversations/<conversation_token>/messages",
        views.ConversationsMessagesAPIView().as_view(),
        name="conversation_messages",
    ),
    path(
        "shares/<group_id>",
        views.NextcloudShareFileConversationAPIView.as_view(),
        name="shares",
    ),
    path(
        "files/content-types",
        views.NextcloudFileTypeAPIView.as_view(),
        name="files_extensions",
    ),
    path(
        "files/<file_id>",
        views.NextcloudFileAPIView.as_view(),
        name="files",
    ),
    path(
        "downloads/<file_id>",
        views.NextcloudDownloadFileAPIView.as_view(),
        name="downloads",
    ),
]
