from apps.nextcloud.resources import users as nextcloud_user_manager


USER_DISPLAY_NAME_PROP = "displayname"


def available_organization_groups():
    """
    Returns a list of available groups assignable to users from an organization.
    """
    return ["OrgOwner", "User", "Admin"]


def delete_user(user_id, nextcloud_user_id, nextcloud_user_password):
    # Remove nextcloud user if exist
    if nextcloud_user_manager.user_exists_in_nextcloud(
        user_id, nextcloud_user_id, nextcloud_user_password
    ):
        nextcloud_user_manager.delete_user(
            user_id=user_id,
            auth_user_id=nextcloud_user_id,
            auth_user_app_password=nextcloud_user_password,
        )


def update_display_name(
    user_id: str,
    display_name: str,
    nextcloud_user_id: str,
    nextcloud_user_password: str,
):
    nextcloud_user_manager.update_user(
        {USER_DISPLAY_NAME_PROP: display_name},
        nextcloud_user_id,
        nextcloud_user_id,
        nextcloud_user_password,
    )
