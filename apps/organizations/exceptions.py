class OrganizationException(Exception):
    """
    Raised when an organization related exception is found.

    Attributes:
        message -- explanation of the error
    """

    def __init__(self, message: str = None) -> None:
        if message:
            self.message = message
        super().__init__(self.message)


class OrganizationTeamCreationException(OrganizationException):
    message: str = "The organization team could not be created."


class OrganizationAddTeamMemberException(OrganizationException):
    message: str = "The user could not added to the team."


class OrganizationRemoveTeamMemberException(OrganizationException):
    message: str = "The user could not be removed form the team."


class OrganizationPersonalWorkspaceCreationException(OrganizationException):
    message: str = "The personal workspace could not be created."


class OrganizationTeamDeletionException(OrganizationException):
    message: str = "The team could not be deleted."


class OrganizationCantInviteMembersException(OrganizationException):
    message: str = "The user does not have permissions to invite members."


class OrganizationShareUploadedFileException(OrganizationException):
    message: str = "The user could not upload the file to the team"


class OrganizationProjectsRetrievalException(OrganizationException):
    message: str = "The user projects could not be retrieved"


class OrganizationProjectNotFoundException(OrganizationException):
    """
    Raised when a project could not be retrieved.
    """

    message: str = "The project could not be found"


class OrganizationProjectDownloadException(OrganizationException):
    """
    Raised when a project download could not be performed.
    """

    message: str = "The project could not be downlaoded"


class OrganizationTeamListRetrievalFailedException(OrganizationException):
    """
    Raised when a the teams list couldn't be retrieved.
    """

    message: str = "The team list could not be retrieved"


class OrganizationSingleTeamRetrievalFailedException(OrganizationException):
    """
    Raised when a Team could not be found.
    """

    message: str = "The team could not be retrieved"


class UserTeamSingleRetrievalFailedException(OrganizationException):
    """
    Raised when a `UserTeam` could not be found.
    """

    error_code: str = "ERRORS_USER_TEAM_DOES_NOT_EXIST"
    message: str = "The user team relationship does not exist"


class OrganizationLeaveTeamFailedException(OrganizationException):
    """
    Raised when an error occurs while a user leaves a Team.
    """

    error_code: str = "ERRORS_LEAVE_TEAM_FAILED"
    message: str = "The user could not leave the team"


class UserCannotLeaveTeamException(OrganizationException):
    """
    Raised when a constraint is met when a user is trying to leave a team.
    """

    error_code: str = "ERRORS_USER_CANNOT_LEAVE_TEAM"
    message: str = None


class OrganizationUpdateUserTeamPermissionsFailedException(OrganizationException):
    """
    Raised when an error occurs when updating a user permissions for a team.
    """

    error_code: str = "ERRORS_UPDATE_USER_TEAM_PERMISSIONS_FAILED"
    message: str = "User permissions for a team could not be updated"


class TeamGroupDoesNotExistException(OrganizationException):
    """
    Raised when a team permissions group could not be found.
    """

    error_code: str = "ERRORS_TEAM_GROUP_DOES_NOT_EXIST"
    message: str = "The given group could not be found or is not available for managing team permissions"


class ListTeamMembersFailedException(OrganizationException):
    """
    Raised when the team members could not be retrieved.
    """

    error_code: str = "ERRORS_LIST_TEAM_MEMBERS_FAILED"
    message: str = "The team members list could not be retrieved."


class GetTeamMemberFailedException(OrganizationException):
    """
    Raised when a team member could not be retrieved.
    """

    error_code: str = "ERRORS_GET_TEAM_MEMBER_FAILED"
    message: str = "The team member could not be retrieved."
