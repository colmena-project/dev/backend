from .models import Organization, UserOrganizationGroupProxy

from apps.accounts.models import User, Language
from apps.invitations import invitations

from colmena.validators.validators import validate_characters

from django import forms
from django.contrib.auth.models import Group
from django.utils.translation import gettext_lazy
from django_countries.widgets import CountrySelectWidget


class OrganizationForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(OrganizationForm, self).__init__(*args, **kwargs)

    invited_by = forms.IntegerField(widget=forms.HiddenInput(), required=True)

    error_messages = {
        "email_exists": gettext_lazy("We have a user with this email"),
        "username_exists": gettext_lazy("We have a user with this username"),
        "invalid_characters": gettext_lazy(
            "This field can only contain letters, numbers, and the following characters: .@-_'"
        ),
    }

    admin_email = forms.EmailField(label=gettext_lazy("admin_user_email"))

    admin_full_name = forms.CharField(
        label=gettext_lazy("admin_full_name"),
        max_length=80,
    )

    admin_username = forms.CharField(
        label=gettext_lazy("admin_username"),
        max_length=30,
    )

    admin_language = forms.ModelChoiceField(
        queryset=Language.objects.all(), label=gettext_lazy("admin_language")
    )

    class Meta:
        model = Organization
        fields = [
            "name",
            "country",
        ]
        widgets = {"country": CountrySelectWidget()}

    fieldsets = (
        (
            None,
            {
                "fields": (
                    "name",
                    "admin_email",
                    "admin_full_name",
                    "admin_language",
                    "country",
                ),
            },
        ),
    )

    def clean_admin_email(self):
        new_email = self.cleaned_data.get("admin_email")
        # Check don't exist user with this email
        if User.objects.filter(email=new_email).exists():
            raise forms.ValidationError(
                self.error_messages["email_exists"],
            )
        return new_email

    def clean_admin_username(self):
        new_username = self.cleaned_data.get("admin_username")
        # Check don't exist user with username
        if validate_characters(new_username):
            raise forms.ValidationError(
                self.error_messages["invalid_characters"],
            )
        if User.objects.filter(username__iexact=new_username).exists():
            raise forms.ValidationError(
                self.error_messages["username_exists"],
            )
        return new_username

    def save(self, commit=True):
        # Handle Errors
        if self.errors:
            raise ValueError(
                "The %s could not be %s because the data didn't validate."
                % (
                    self.instance._meta.object_name,
                    "created" if self.instance._state.adding else "changed",
                )
            )
        cleaned_data = self.cleaned_data
        org_owner_group: Group = Group.objects.get(name="OrgOwner")
        # Create new Organization
        self.instance.language = cleaned_data["admin_language"]
        self.instance.save()

        # Create new User
        new_user: User = User.objects.create(
            username=cleaned_data["admin_username"],
            email=cleaned_data["admin_email"],
            full_name=cleaned_data["admin_full_name"],
        )
        new_user.save()
        new_user.language.add(cleaned_data["admin_language"])
        new_user.groups.add(org_owner_group)
        # Create UserOrganizationProxy
        UserOrganizationGroupProxy.objects.create(
            user=new_user,
            organization=self.instance,
            group=org_owner_group,
        )

        # Generates a new organization invitation
        invited_by_id = cleaned_data["invited_by"]
        invited_by: User = User.objects.get(id=invited_by_id)
        organization = self.instance
        invitations.create_organization_invitation(
            new_user, invited_by, organization, org_owner_group
        )

        return self.instance

    def save_m2m(self):
        return self._save_m2m()
