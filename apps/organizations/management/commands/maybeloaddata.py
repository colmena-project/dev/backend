from django.core.management.commands.loaddata import Command as Loaddata


class Command(Loaddata):
    def save_obj(self, obj):
        app = obj.object.__class__
        fixture_pk = obj.object.pk
        exist = app.objects.filter(pk=fixture_pk).exists()

        if exist:
            return False
        else:
            return super().save_obj(obj)
