"""
This module provides an API to interact with specific functions of the
organizations API, such as retrieving entities from a model, interacting with
organizations APIs or even perform validations.
This module is considered the entrypoint to the organizations bussiness model.
"""

import apps.nextcloud
import logging
import uuid

from django.apps import apps
from django.contrib.auth.models import Group
from django.core.exceptions import ValidationError
from django.db.models import Q
from django.db.models.base import Model
from django.db.models.manager import BaseManager
from apps.accounts import accounts
from apps.accounts.models import OrganizationMemberInvitation, User, get_super_admin

from apps.invitations.exceptions import UnauthorizedUserException

from apps.nextcloud.resources import groups as nextcloud_group_manager
from apps.nextcloud.resources import users as nextcloud_user_manager

from apps.invitations.models import TeamInvitation
from .exceptions import (
    OrganizationAddTeamMemberException,
    OrganizationCantInviteMembersException,
    OrganizationTeamCreationException,
    OrganizationSingleTeamRetrievalFailedException,
    OrganizationUpdateUserTeamPermissionsFailedException,
    UserTeamSingleRetrievalFailedException,
    TeamGroupDoesNotExistException,
    ListTeamMembersFailedException,
    GetTeamMemberFailedException,
)

from .models import (
    FavouriteTeam,
    Organization,
    Team,
    UserOrganizationGroup,
    UserOrganizationGroupProxy,
    UserTeam,
)
from .resources import team as organization_team_manager
from .resources import shares as shares_manager

logger = logging.getLogger(__name__)


# ------------------------------------------------------------------------------
# Model accesors
#


# ------------------------------------------------------------------------------
# Organizations module API
#


def add_members(
    user: User,
    organization_id: int,
    team_id: int,
    new_users: list,
) -> None:
    """
    Given a `user`, an `organization_id` a `team_id` and a list of `new_users`,
    adds each user to the organization team.
    """
    team = get_team(team_id=team_id, organization_id=organization_id)
    users_in_team = [
        user_team.user for user_team in list(UserTeam.objects.filter(team_id=team.id))
    ]
    user_organization_group_list = (
        UserOrganizationGroup.objects.filter(
            user__in=new_users, organization_id=organization_id
        )
        .exclude(user__in=list(users_in_team) + [user])
        .distinct("user")
    )
    organization_users = [
        user_organization_group.user
        for user_organization_group in list(user_organization_group_list)
    ]

    # Add new members as Team collaborators by default
    team_group: Group = Group.objects.get(name="TeamCollaborator")

    for _user in organization_users:
        organization_team_manager.add_member(
            user=_user,
            team_group=team_group,
            nextcloud_user_id=_user.username,
            team=team,
            invited_by=user,
            superadmin_user=get_super_admin(),
        )


def remove_member(user: User, team_id: int, user_id: int) -> None:
    """
    Given a `user` that requests a deletion, a `team_id` and a `user_id`,
    removes the user from a team.
    """
    team: Team = get_team_by_id(team_id=team_id)
    targeted_user = accounts.get_user_by_id(user_id=user_id)

    organization_team_manager.remove_from_team(
        user=targeted_user, team=team, team_admin=user
    )


def leave_team(user: User, team_id: int) -> None:
    """
    Given a `user` and a `team`, impersonates the user to leave the team.
    """
    team: Team = get_team_by_id(team_id=team_id)
    user_team: UserTeam = get_user_team(user_id=user.id, team_id=team.id)
    organization_team_manager.leave_team(user_team=user_team)
    user_team.delete()


def create_team(
    user: User,
    organization_id: int,
    name: str,
    description: str,
    logo: str,
    users: list,
    new_users: list,
):
    """
    Given a user, an organization id, a team name, description and logo, returns a new Team.

    Args:
        - user: User instance
        - organization_id: int
        - name: str
        - description: str
        - logo: str
        - users: list[int] A list of users to invite to the team that already belong to the given organization
        - new_users: list[int] A list of users to invite to the given organization and team

    Returns:
        Team: Team instance

    Raises:
        ValidationError: If validation fails or user lacks permissions
    """

    try:
        # Creates the Team
        team = organization_team_manager.create(
            group_name=str(uuid.uuid4()),
            nextcloud_user_id=user.get_nextcloud_user_id(),
            organization=get_user_organization(user, organization_id),
            team_name=name,
            user=user,
            team_description=description,
            team_logo=logo,
        )

        # Invite users if any
        user_organization_group_list = UserOrganizationGroup.objects.filter(
            user__in=users, organization_id=organization_id
        ).distinct("user")
        users = [
            user_organization_group.user
            for user_organization_group in list(user_organization_group_list)
        ]

        team_group: Group = Group.objects.get(name="TeamCollaborator")
        if len(users) > 0:
            for _user in users:
                if not _user_belongs_to_team(user=_user, team=team):
                    organization_team_manager.add_member(
                        user=_user,
                        team_group=team_group,
                        nextcloud_user_id=_user.username,
                        team=team,
                        invited_by=user,
                        superadmin_user=get_super_admin(),
                    )

        for user_id in new_users:
            _user = User.objects.get(id=user_id)
            TeamInvitation.objects.create(
                user=_user,
                group=team_group,
                team=team,
                invited_by=user,
            )

        team_data = organization_team_manager.populate_team_data(
            team=team, auth_user=user, shared_files=True, last_message=True
        )

        return team_data

    except OrganizationTeamCreationException as e:
        raise e


def list_team_members(team_id: int, exclude_users=[]) -> list[dict[str, User | Group]]:
    """
    Given a `team_id` returns a list of members.
    """
    try:
        team: Team = get_team_by_id(team_id=team_id)

        user_teams: BaseManager[UserTeam] = (
            UserTeam.objects.filter(team=team)
            .exclude(user__id__in=[user.id for user in exclude_users])
            .select_related("user", "group")
            .exclude()
        )
        team_members: list[dict[str, User | Group]] = [
            {"user": user_team.user, "group": user_team.group}
            for user_team in user_teams
        ]
        return team_members
    except OrganizationSingleTeamRetrievalFailedException as e:
        logger.error(
            "Cannot list team members, the team could not be found. Exception=%s", e
        )
        raise ListTeamMembersFailedException(e.message) from e


def get_team_member(team_id: int, member_id: int) -> dict[str, User | Group]:
    """
    Given a `team_id` and a `member_id` returns a team member.
    """
    try:
        team: Team = get_team_by_id(team_id=team_id)
        user: User = User.objects.get(id=member_id)

        user_team: BaseManager[UserTeam] = UserTeam.objects.get(team=team, user=user)
        return {"user": user_team.user, "group": user_team.group}

    except OrganizationSingleTeamRetrievalFailedException as e:
        logger.error("Cannot retrieve team member. Exception=%s", e)
        raise GetTeamMemberFailedException(e.message) from e


def _user_belongs_to_team(user, team) -> bool:
    return UserTeam.objects.filter(user=user, team=team).exists()


def get_user_organization(user: User, organization_id: int) -> Organization:
    """
    Returns a list of UserOrganizationGroup
    """
    return UserOrganizationGroup.objects.get(
        user=user, organization_id=organization_id
    ).organization


def list_organizations() -> Organization:
    """
    Returns a list of organization
    """
    return Organization.objects.all()


def get_organization(organization_id: int) -> Organization:
    """
    Returns an organization if exists.
    """
    return Organization.objects.get(id=organization_id)


def _organization_exist(organization_id: int) -> Organization:
    return Organization.objects.filter(id=organization_id).exists()


def create_organization_member(
    username: str, fullname: str, email: str, organization: Organization, group_id: int
) -> tuple[OrganizationMemberInvitation, Organization, Group]:
    """
    Given some user details, an organization and a group, creates and invites
    a user to the given organization.
    """
    group: Group = Group.objects.get(id=group_id)

    organization_member: OrganizationMemberInvitation = (
        accounts.create_organization_member_invitation(
            username=username, fullname=fullname, email=email
        )
    )
    organization_member.language.add(organization.language)
    organization_member.groups.add(group)
    create_user_organization_group(
        member=organization_member, organization=organization, group=group
    )

    return organization_member, organization, group


def create_user_organization_group(
    member: OrganizationMemberInvitation, organization: Organization, group: Group
) -> UserOrganizationGroupProxy:
    """
    Given an organization invitation, an `organization` and a group, creates
    the relationship between them.
    """
    return UserOrganizationGroupProxy.objects.create(
        user=member,
        organization=organization,
        group=group,
    )


def validate_invitation_permission(user: User, organization: Organization) -> bool:
    """
    Given a `user` and an `organization`, returns true if the user has been
    invited to the organization.
    """
    if not UserOrganizationGroupProxy.objects.filter(
        Q(
            user=user,
            organization=organization.id,
            group=Group.objects.get(name="Admin"),
        )
        | Q(
            user=user,
            organization=organization.id,
            group=Group.objects.get(name="OrgOwner"),
        )
    ).exists():
        raise OrganizationCantInviteMembersException


def user_exists_in_organization(user_id) -> bool:
    """
    Returns true when the user exists in an organization.
    """
    user_organization_group_proxy_model: Model = apps.get_model(
        "organizations", "UserOrganizationGroupProxy"
    )
    return user_organization_group_proxy_model.objects.filter(user__id=user_id).exists()


def get_personal_workspace(nextcloud_user_id):
    """
    Given a `nextcloud_user_id` returns the personal workspace, if exists.
    """
    team_model: Model = apps.get_model("organizations", "Team")

    user: User = accounts.get_user(nextcloud_user_id)

    try:
        return team_model.objects.get(
            userteam__user_id=user.id, is_personal_workspace=True
        )
    except Exception as e:
        raise ValidationError("USER_GET_PERSONAL_WORKSPACE_TEAM_FAILED") from e


def get_team(team_id: int, organization_id: int) -> Team:
    """
    Given an `id` returns a Team model, if exists. Otherwise raises an
    OrganizationSingleTeamRetrievalFailedException exception.
    """
    team_model: type[Model] = apps.get_model(
        app_label="organizations", model_name="Team"
    )

    try:
        return team_model.objects.get(
            id=team_id, is_personal_workspace=False, organization_id=organization_id
        )
    except Exception as e:
        logger.error("The team could not be found. Exception=%s", e)
        raise OrganizationSingleTeamRetrievalFailedException from e


def get_team_by_id(team_id: int) -> Team:
    """
    Given a `team_id` returns a Team model, if exists. Otherwise raises an
    OrganizationSingleTeamRetrievalFailedException exception.
    """
    team_model: type[Model] = apps.get_model(
        app_label="organizations", model_name="Team"
    )

    try:
        return team_model.objects.get(id=team_id, is_personal_workspace=False)
    except Exception as e:
        logger.error("The team could not be found. Exception=%s", e)
        raise OrganizationSingleTeamRetrievalFailedException from e


def get_user_team(user_id: int, team_id: int) -> Team:
    """
    Given a `user_id` and a `team_id`, returns a `UserTeam` model, if exists.
    Otherwise raises a UserTeamSingleRetrievalFaieldException exception.
    """
    team_model: type[Model] = apps.get_model(
        app_label="organizations", model_name="UserTeam"
    )

    try:
        return team_model.objects.get(team__id=team_id, user__id=user_id)
    except Exception as e:
        logger.error("The user team could not be found. Exception=%s", e)
        raise UserTeamSingleRetrievalFailedException from e


def initialise_groups(
    user: User,
    organization: Organization,
    group: Group,
    nextcloud_user_id: str,
    invited_by: User,
    extra_teams_ids: list,
) -> None:
    """
    Given a `user`, an `organization`, a `group`, a `nextcloud_user_id` checks
    invitation permissions to then either create the initial organization groups
    or invite the `user` to existing organization groups.
    """
    match group:
        case Group(name="OrgOwner"):
            organization_team_manager.create(
                nextcloud_group_manager.GENERAL_GROUP_NAME,
                nextcloud_user_id,
                organization,
                organization.name,
                user,
            )

        case Group(name="Admin") | Group(name="User"):
            team_group: Group = _get_team_group_by_organization_group(
                organization_group=group
            )

            try:
                nc_group_id: str = nextcloud_group_manager.build_group_identifier(
                    org_id=organization.id,
                    team_identifier=nextcloud_group_manager.GENERAL_GROUP_NAME,
                )
                team: Team = Team.objects.get(nc_group_id=nc_group_id)
                organization_team_manager.add_member(
                    user=user,
                    team_group=team_group,
                    nextcloud_user_id=nextcloud_user_id,
                    team=team,
                    invited_by=invited_by,
                    superadmin_user=get_super_admin(),
                )

            except OrganizationAddTeamMemberException as e:
                superadmin_user = get_super_admin()
                nextcloud_user_manager.delete_user(
                    user_id=nextcloud_user_id,
                    auth_user_id=superadmin_user.get_nextcloud_user_id(),
                    auth_user_app_password=superadmin_user.nc_app_password,
                )
                raise OrganizationAddTeamMemberException from e

    organization_team_manager.create_personal_workspace(
        user,
        nextcloud_user_id,
    )
    for team_id in extra_teams_ids:
        add_members(
            user=invited_by,
            organization_id=organization.id,
            team_id=team_id,
            new_users=[user],
        )


def _get_team_group_by_organization_group(organization_group: Group) -> Group:
    match organization_group:
        case Group(name="Admin"):
            team_group: Group = Group.objects.get(name="TeamAdmin")

        case Group(name="User"):
            team_group: Group = Group.objects.get(name="TeamCollaborator")

    return team_group


def download_project(file_id: int, user: User) -> bytes | bytearray | memoryview:
    """
    Given an organisation identifier, returns the members of an organisation grouped into active and suspended.
    """
    return shares_manager.download_project(file_id, user)


def user_has_any_role_in_organization(
    user: User, roles: list, organization_id: int
) -> bool:
    return UserOrganizationGroup.objects.filter(
        group__name__in=roles, organization_id=organization_id, user_id=user.pk
    ).exists()


def user_has_any_role_in_team(user: User, roles: list, team_id: int) -> bool:
    """
    Given a `user`, a list of team `roles` and a `team_id`, returns a boolean
    representing whether the user has permissions or not.
    """
    return UserTeam.objects.filter(
        group__name__in=roles, user_id=user.pk, team_id=team_id
    ).exists()


def get_organization_members(organization_id: int, user: User) -> tuple[list, list]:
    """
    Given an organization id, returns a tuple where the first element is a list
    of active users, and the second element is a list of suspended users.
    The suspended feature is currently not implemented, thus an empty list is
    assigned.
    """

    if not user_has_any_role_in_organization(
        user, ["Admin", "OrgOwner"], organization_id
    ):
        raise UnauthorizedUserException

    organization_members: list = UserOrganizationGroup.objects.filter(
        organization_id=organization_id
    )

    # FIXME: When you have the functionality to suspend users, you must check
    # the suspension status.
    query = {"user__nc_app_password": ""}
    active_members: list = organization_members.exclude(**query)
    # FIXME: When there is a functionality to suspend users, it will be replaced
    # by the list of suspended users.
    suspended_members: list = []

    return active_members, suspended_members


def get_team_groups() -> list[Group]:
    """
    Returns a list of available groups for teams management
    """
    return list(
        Group.objects.filter(
            Q(name="TeamOwner") | Q(name="TeamAdmin") | Q(name="TeamCollaborator")
        )
    )


def update_user_team_permissions(team_id: int, user_id: int, group_id: int, user: User):
    """
    Given a user that performs an action, a `team_id` a `user_id` and a
    `group_id`, updates team permissions for the given user with `user_id`.
    """
    try:
        group: Group = _get_team_group(group_id=group_id)
        user_team: UserTeam = organization_team_manager.update_user_team_group(
            user_team=get_user_team(team_id=team_id, user_id=user_id), group=group
        )
        return user_team

    except (
        UserTeamSingleRetrievalFailedException,
        TeamGroupDoesNotExistException,
    ) as e:
        logger.error(
            "An error occurred while updating the user team group. Exception=%s", e
        )
        raise OrganizationUpdateUserTeamPermissionsFailedException(
            message=e.message
        ) from e


def _get_team_group(group_id: int) -> Group:
    try:
        avaialble_group_ids: list[int] = [group.id for group in get_team_groups()]
        return Group.objects.get(id=group_id, id__in=avaialble_group_ids)
    except Group.DoesNotExist as e:
        raise TeamGroupDoesNotExistException from e


def get_user_team_group(user_id, team_id) -> Group:
    """
    Given a `user_id` and a `team_id` returns the group for the given relationship
    """
    return get_user_team(user_id=user_id, team_id=team_id).group


# ------------------------------------------------------------------------------
# Team permission groups
#


def get_team_owner_groups() -> list[Group]:
    """
    Returns a list of groups that TeamOwner users can manage.
    """
    return list(
        Group.objects.filter(
            Q(name="TeamOwner") | Q(name="TeamAdmin") | Q(name="TeamCollaborator")
        )
    )


def get_team_admin_groups() -> list[Group]:
    """
    Returns a list of groups that TeamAdmin users can manage.
    """
    return list(Group.objects.filter(Q(name="TeamAdmin") | Q(name="TeamCollaborator")))


def get_teams_by_user(member_id: int) -> list[UserTeam]:
    """
    Given a `member_id`, returns a list of team and groups where the given user
    belongs.
    """
    return list(
        UserTeam.objects.filter(
            user_id=member_id, team__is_personal_workspace=False, team__deleted=False
        ).select_related("team", "group")
    )


def delete_team(team: Team) -> None:
    try:
        team.delete_team()
        FavouriteTeam.objects.filter(team=team).delete()

    except Exception as e:
        logger.error(
            "Unexpected error while trying to delete the team. Exception=%s", e
        )
        raise Exception("ERRORS_UNEXPECTED_ERROR_ON_TEAM_DELETION")
