"""
Permissions module for the organization views.
"""

import logging

from django.contrib.auth.models import Group

from rest_framework.permissions import BasePermission
from apps.accounts.accounts import get_user_by_id
from apps.accounts.models import User
from apps.organizations.organizations import (
    get_user_team_group,
    user_has_any_role_in_organization,
    user_has_any_role_in_team,
    get_team_owner_groups,
    get_team_admin_groups,
)
from apps.organizations.models import Organization

logger = logging.getLogger(__name__)


class IsAdminOrOrgOwner(BasePermission):
    """
    Allows access only to Admin or OrgOwner users in the given organization.
    """

    def has_permission(self, request, view) -> bool:
        """
        Return `True` if user is Admin or OrgOwner in the given organization
        """

        organization_id = view.kwargs.get("organization_id") or request.data.get(
            "organization_id"
        )
        return user_has_any_role_in_organization(
            user=request.user,
            roles=["Admin", "OrgOwner"],
            organization_id=organization_id,
        )


class TeamPermissionChecker(BasePermission):
    """
    Checks if the user is trying to perform an action over a user with more
    privileges that itself.
    """

    def has_permission(self, request, view) -> bool:
        """
        Returns `True` if the user that is requesting a change over another
        user has enough privileges to perform such action.
        """
        team_id = (
            view.kwargs.get("team_id")
            or view.kwargs.get("pk")
            or request.data.get("team_id")
        )

        if user_has_any_role_in_team(
            user=request.user, roles=["TeamOwner"], team_id=team_id
        ):
            return True

        if user_has_any_role_in_team(
            user=request.user, roles=["TeamAdmin"], team_id=team_id
        ):
            try:
                user_id = request.data.get("user_id") or view.kwargs.get("user_id")
                targeted_user: User = get_user_by_id(user_id=user_id)
                return user_has_any_role_in_team(
                    user=targeted_user,
                    roles=["TeamAdmin", "TeamCollaborator"],
                    team_id=team_id,
                )

            except User.DoesNotExist as e:
                logger.error("The user does not exist. Exception=%s", e)
                return False

        return False


class IsTeamOwnerOrTeamAdmin(BasePermission):
    """
    Allows access only to TeamAdmin or TeamOwner users in the given team.
    """

    def has_permission(self, request, view) -> bool:
        """
        Return `True` if user is TeamAdmin or TeamOwner in the given team.
        """

        team_id = (
            view.kwargs.get("team_id")
            or view.kwargs.get("pk")
            or request.data.get("team_id")
        )

        return user_has_any_role_in_team(
            user=request.user, roles=["TeamAdmin", "TeamOwner"], team_id=team_id
        )


class IsTeamMember(BasePermission):
    """
    Checks whether a user is member of a team of not.
    """

    def has_permission(self, request, view) -> bool:
        team_id = (
            view.kwargs.get("team_id")
            or view.kwargs.get("pk")
            or request.data.get("team_id")
        )

        return user_has_any_role_in_team(
            user=request.user,
            roles=["TeamAdmin", "TeamOwner", "TeamCollaborator"],
            team_id=team_id,
        )


class CanCreateTeam(IsAdminOrOrgOwner):
    """
    Determines whether a user can create a team or not
    """

    def has_permission(self, request, view) -> bool:
        if view.action not in ["create"]:
            return True

        return super().has_permission(request, view)


class CanInviteMembers(IsTeamOwnerOrTeamAdmin):
    """
    Determines whether a user can invite members or not
    """

    def has_permission(self, request, view) -> bool:
        if view.action not in ["add_members"]:
            return True

        return super().has_permission(request, view)


class CanRemoveMember(TeamPermissionChecker):
    """
    Determines whether a user can remove a member or not
    """

    def has_permission(self, request, view) -> bool:
        if view.action not in ["remove_member"]:
            return True

        return super().has_permission(request, view)


class CanLeaveTeam(BasePermission):
    """
    Determines whether a user can leave a team or not
    """

    def has_permission(self, request, view) -> bool:
        if view.action not in ["leave_team"]:
            return True

        team_id = (
            view.kwargs.get("team_id")
            or view.kwargs.get("pk")
            or request.data.get("team_id")
        )

        return user_has_any_role_in_team(
            user=request.user,
            roles=["TeamOwner", "TeamAdmin", "TeamCollaborator"],
            team_id=team_id,
        )


class CanUpdateTeam(IsTeamOwnerOrTeamAdmin):
    """
    Determines whether a user can invite a member or not
    """

    def has_permission(self, request, view) -> bool:
        if view.action not in ["partial_update"]:
            return True

        return super().has_permission(request, view)


class CanListTeamMembers(IsTeamMember):
    """
    Determines whether a user can retrieve a team members list.
    """

    def has_permission(self, request, view) -> bool:
        if view.action not in ["list_members"]:
            return True

        return super().has_permission(request, view)


class CanRetrieveTeamMember(IsTeamMember):
    """
    Determines whether a user can retrieve team member details.
    """

    def has_permission(self, request, view) -> bool:
        if view.action not in ["get_member"]:
            return True

        return super().has_permission(request, view)


class CanUpdateUserTeamPermissions(TeamPermissionChecker):
    """
    Determines whether a user can update another user's permissions for a team
    or not.
    """

    def has_permission(self, request, view) -> bool:
        if view.action not in ["update_team_permissions"]:
            return True

        user_id = request.data.get("user_id") or view.kwargs.get("user_id")
        is_targeting_current_user: bool = int(user_id) == int(request.user.id)

        team_id = (
            view.kwargs.get("team_id")
            or view.kwargs.get("pk")
            or request.data.get("team_id")
        )

        group_id = request.data.get("group_id")
        group: Group = Group.objects.get(id=group_id)

        return (
            not is_targeting_current_user
            and super().has_permission(request, view)
            and _check_permissions_by_target_group(
                user_id=request.user.id, team_id=team_id, group=group
            )
        )


def _check_permissions_by_target_group(user_id: int, team_id, group: Group) -> bool:
    user_team_group: Group = get_user_team_group(user_id=user_id, team_id=team_id)

    match user_team_group:
        case Group(name="TeamOwner"):
            return group in get_team_owner_groups()

        case Group(name="TeamAdmin"):
            return group in get_team_admin_groups()

        case Group(name="TeamCollaborator"):
            return False

        case _:
            return False


class IsTeamOwner(BasePermission):
    """
    Allows access only to TeamOwner users in the given team.
    """

    def has_permission(self, request, view) -> bool:
        """
        Return `True` if user is TeamOwner in the given team.
        """

        team_id = (
            view.kwargs.get("team_id")
            or view.kwargs.get("pk")
            or request.data.get("team_id")
        )

        return user_has_any_role_in_team(
            user=request.user, roles=["TeamOwner"], team_id=team_id
        )


class CanDeleteTeam(IsTeamOwner):
    """
    Determines whether a user can delete a Team or not
    """

    def has_permission(self, request, view) -> bool:
        if view.action not in ["destroy"]:
            return True
        return super().has_permission(request, view)


class BelongsToOrganization(BasePermission):
    """
    Determines whether a user belongs to an organization or not.
    """

    def has_permission(self, request, view) -> bool:
        organization_id = request.parser_context["kwargs"].get("organization_id")

        if not organization_id:
            return False
        try:
            organization = Organization.objects.get(id=organization_id)
        except Organization.DoesNotExist:
            return False
        return user_has_any_role_in_organization(
            user=request.user,
            roles=["Admin", "OrgOwner", "User"],
            organization_id=organization.id,
        )
