"""
Convenience module that encapsulates the `shares` bussiness model for the
organizations app.
"""

import logging

from django.core.exceptions import ValidationError

from apps.accounts.models import User
from apps.nextcloud import files as nextcloud_file_manager
from apps.nextcloud.config import get_talk_dir, get_projects_dir
from apps.nextcloud.exceptions import (
    NextcloudUploadFileException,
    NextcloudGetProjectsException,
    NextcloudProjectNotFoundException,
    NextcloudDownloadFileException,
)
from apps.nextcloud.resources import files
from apps.organizations.exceptions import (
    OrganizationShareUploadedFileException,
    OrganizationProjectsRetrievalException,
    OrganizationProjectNotFoundException,
    OrganizationProjectDownloadException,
)
from apps.organizations.resources.team import get_team_by_nc_group_id
from colmena.serializers.serializers import ErrorSerializer

logger = logging.getLogger(__name__)

TALK_DIR = get_talk_dir()
PROJECTS_DIR = get_projects_dir()
PERSONAL_WORKSPACE_TEAM_NAME = "Personal Workspace"


def upload(new_file, filename, teams, user):
    """
    Given a file, a list of teams and a user, uploads and shares the file to
    the given team conversations.

    Parameters
    ----------
    new_file : InMemoryUploadedFile
        A file in memory.

    new_file : str
        A file name

    teams : List[Team]
        A list of Team the current user belongs to

    user : User
        The user model.

    Returns
    -------
        None

    Raises
    ------
    ValidationError
        When any nextcloud operation fails

    """
    try:
        # Upload file
        upload_response = nextcloud_file_manager.upload(
            new_file, filename, TALK_DIR, user
        )

        _share_uploaded_file(upload_response["nextcloud_upload_path"], teams, user)

    except Exception as e:
        raise ValidationError(ErrorSerializer("ERRORS_SHARE_FILE_FAILED").data) from e


def upload_project(new_file, filename, teams, user):
    """
    Given a file, a list of teams and a user, uploads and shares the project to
    the given team conversations.

    Parameters
    ----------
    new_file : InMemoryUploadedFile
        A file in memory.

    new_file : str
        A file name

    teams : List[Team]
        A list of Team the current user belongs to

    user : User
        The user model.

    Returns
    -------
        None

    Raises
    ------
    ValidationError
        When any nextcloud operation fails

    """
    try:
        _maybe_create_projects_folder(user)
        upload_response = nextcloud_file_manager.upload(
            new_file, filename, PROJECTS_DIR, user
        )
        _share_uploaded_file(upload_response["nextcloud_upload_path"], teams, user)

    except NextcloudUploadFileException as e:
        logger.error(
            "Failed uploading project",
            extra={
                "auth_user_id": user.id,
                "teams": teams,
                "filename": filename,
            },
        )
        raise OrganizationShareUploadedFileException from e


def list_projects(user: User):
    """
    Given a user to authenticate the request, returns all user projects.

    Parameters
    ----------

    user : User
        A user model. This user is assumed to have the required
        privileges to perform nextcloud requests with an app password.

    Returns
    -------
    [
        {
            "id": int,
            "name": str,
            "shared_teams":[
                {
                    "team_id": int,
                    "name": str
                },
            ]
        },
    ]

    """
    try:
        projects = nextcloud_file_manager.get_projects_by_user(user)
        for project in projects:
            shared_teams = project["shared_teams"]
            project["shared_teams"] = [
                _hydrate_team_by_team_name(team_name) for team_name in shared_teams
            ]
        return projects
    except NextcloudGetProjectsException as e:
        logger.error(
            "Failed fetching the list of user projects",
            extra={
                "auth_user_id": user.id,
            },
        )
        raise OrganizationProjectsRetrievalException from e


def download_project(file_id: int, user: User):
    """
    Given a file_id and a user to authenticate the request,
    downloads a file that represents a project.


    Parameters
    ----------

    `file_id` : int
        A file id.

    `user` : User
        A user model. This user is assumed to have the required
        privileges to perform nextcloud requests with an app password.

    Returns
    -------
    `ByteString`

    Raises
    -------
    `OrganizationProjectNotFoundException` | `OrganizationProjectDownloadException`


    """
    try:
        return nextcloud_file_manager.download_project(file_id, user)
    except NextcloudProjectNotFoundException as e:
        raise OrganizationProjectNotFoundException from e
    except NextcloudDownloadFileException as e:
        raise OrganizationProjectDownloadException from e


def _share_uploaded_file(nextcloud_upload_path, teams, user):
    # Share uploaded file
    for team in teams:
        nextcloud_file_manager.share(
            nextcloud_upload_path,
            team.nc_conversation_token,
            user,
        )


def _hydrate_team_by_team_name(team_name: str):
    team = get_team_by_nc_group_id(team_name)
    return {
        "team_id": team.id,
        "name": team.name,
        "is_personal_workspace": team.is_personal_workspace,
    }


def _maybe_create_projects_folder(user):
    try:
        files.create_projects_folder(user=user, username=user.username)
    except Exception:
        logger.warning("Projects directory already exists, skipping creation")
