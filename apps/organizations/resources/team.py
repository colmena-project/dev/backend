import logging

from django.core.exceptions import ValidationError
from django.contrib.auth.models import Group
from django.db.models import Case, When, IntegerField, Value

from apps.accounts.models import get_super_admin, User

from apps.nextcloud import nextcloud as nextcloud_manager
from apps.nextcloud.exceptions import (
    NextcloudGroupCreationException,
    NextcloudConversationCreationException,
    NextcloudUserConversationsFetchFailedException,
    NextcloudLeaveConversationFailedException,
    NextcloudRemoveUserFromGroupFailedException,
)
from apps.nextcloud.resources import groups as nextcloud_group_manager
from apps.nextcloud.resources import talk as nextcloud_talk_manager
from apps.nextcloud.resources import users as nextcloud_user_manager
from apps.nextcloud.serializers import NextcloudLastConversationMessageSerializer

from apps.organizations.exceptions import (
    OrganizationAddTeamMemberException,
    OrganizationRemoveTeamMemberException,
    OrganizationTeamCreationException,
    OrganizationTeamDeletionException,
    OrganizationPersonalWorkspaceCreationException,
    OrganizationTeamListRetrievalFailedException,
    OrganizationLeaveTeamFailedException,
)
from apps.organizations.models import Organization, Team, UserTeam
from apps.organizations.serializers import TeamSerializer

from colmena.serializers.serializers import ErrorSerializer


logger = logging.getLogger(__name__)


def create(
    group_name: str,
    nextcloud_user_id: int,
    organization: Organization,
    team_name: str,
    user: User,
    team_description: str = "",
    team_logo: str = "",
) -> Team:
    """
    Given a group name, a nextcloud user id, an organization id and a user, creates a nextcloud
    group and a conversation and adds the user to them. Finally creates an Team model instance
    and associates it to the given organization id.

    Parameters
    ----------
    group_name : str
        The group name

    nextcloud_user_id : str
        The nextcloud user id

    organization : Organization
        The organization model.

    team_name : str
        The team name

    user : User
        The user model.

    team_description: str, optional
        The team description.

    team_logo: str, optional
        Team Logo.


    Returns
    -------
    dict

    Raises
    ------
    OrganizationTeamCreationException
        If the team creation fails.
    """
    # Setup
    superadmin_user = get_super_admin()
    nc_group_id = nextcloud_group_manager.build_group_identifier(
        org_id=organization.id, team_identifier=group_name.lower().replace(" ", "-")
    )

    try:
        # Create a nextcloud group
        nextcloud_group_manager.create_group(group_id=nc_group_id, user=superadmin_user)

        # Initialise conversation
        nc_conversation_token = nextcloud_manager.initialise_conversation(
            group_name=group_name,
            group_id=nc_group_id,
            nextcloud_user_id=nextcloud_user_id,
            user=user,
            auth_user=superadmin_user,
        )

        # Create a new team
        team: Team = Team.objects.create(
            nc_group_id=nc_group_id,
            nc_conversation_token=nc_conversation_token,
            organization=organization,
            name=team_name,
            description=team_description,
            logo=team_logo,
        )

        # Create a UserTeam with TeamOwner group
        team_owner_group: Group = Group.objects.get(name="TeamOwner")
        UserTeam.objects.create(user=user, team=team, group=team_owner_group)

        return team

    except Exception as e:
        # Remove a group
        if nextcloud_group_manager.get_group(group_id=group_name, user=superadmin_user):
            nextcloud_group_manager.remove_group(
                group_id=group_name, user=superadmin_user
            )

        # Remove a team if was created
        try:
            team.delete()
        except UnboundLocalError:
            pass

        raise OrganizationTeamCreationException from e


def add_member(
    user: User,
    team_group: Group,
    nextcloud_user_id: str,
    team: Team,
    invited_by: User,
    superadmin_user: User,
):
    """
    Given a user, a group, a nextcloud user id, a team, an organization owner
    user and a superadmin user to authenticate the request, adds a user to the
    given team.

    Parameters
    ----------
    user : User
        The user model

    team_group : Group
        The group model assigned to the UserTeam instance

    nextcloud_user_id : str
        The nextcloud user id

    team : Team
        The team model

    invited_by : User
        The user model. This user is assumed to have the required
        privileges to add users to a nextcloud group.

    superadmin_user : User
        The user model. This user is assumed to have the required
        privileges to promote users to subadmin.

    Returns
    -------
        None

    Raises
    ------
    ValidationError
        If the any operation could not be performed in nextcloud
    """
    try:
        # Add user to group
        nextcloud_group_manager.add_user_to_group(
            group_id=team.nc_group_id,
            user_id=nextcloud_user_id,
            user=invited_by,
        )

        # Add a UserTeam with TeamCollaborator group
        UserTeam.objects.create(user=user, team=team, group=team_group)

        if team_group.name == "TeamAdmin":
            # Provides conversation creation privileges to the Admin
            nextcloud_group_manager.promote_user_to_subadmin(
                group_id=team.nc_group_id,
                user_id=nextcloud_user_id,
                user=superadmin_user,
            )
    except Exception as e:
        logger.error(
            "Unexpected error while adding member to a team, Exception=%s",
            e,
            extra={"new_user": user.id},
        )
        nextcloud_user_manager.delete_user(
            user_id=user.get_nextcloud_user_id(),
            auth_user_id=superadmin_user.get_nextcloud_user_id(),
            auth_user_app_password=superadmin_user.nc_app_password,
        )
        raise OrganizationAddTeamMemberException from e


def create_personal_workspace(
    user: User,
    nextcloud_user_id: str,
    team_description: str = "",
    team_logo: str = "",
):
    """
    Given a user and a nextcloud user id, creates a Team and a conversation in nextcloud for the given user.

    Parameters
    ----------
    user : User
        The user model

    nextcloud_user_id : str
        The nextcloud user id

    team_description: str, optional
        The team description.

    team_logo: str, optional
        The team logo.

    Returns
    -------
        None

    Raises
    ------
    ValidationError
        When any nextcloud operation fails
    """
    # Setup
    superadmin_user: User = get_super_admin()
    nc_group_id: str = nextcloud_group_manager.build_personal_workspace_name(
        user_id=nextcloud_user_id
    )

    try:
        # Create a nextcloud personal group
        nextcloud_group_manager.create_group(group_id=nc_group_id, user=superadmin_user)

        # Initialise conversation
        nc_conversation_token = nextcloud_manager.initialise_conversation(
            group_name=f"{user.username} workspace",
            group_id=nc_group_id,
            nextcloud_user_id=nextcloud_user_id,
            user=user,
            auth_user=superadmin_user,
        )

        # Create a new team
        team: Team = Team.objects.create(
            name=user.username,
            nc_group_id=nc_group_id,
            nc_conversation_token=nc_conversation_token,
            is_personal_workspace=True,
            description=team_description,
            logo=team_logo,
        )

        # Create a UserTeam with TeamOwner group
        team_owner_group: Group = Group.objects.get(name="TeamOwner")
        UserTeam.objects.create(user=user, team=team, group=team_owner_group)

    except NextcloudGroupCreationException:
        # TODO: add error logs
        _rollback_user_creation(user, superadmin_user)
        raise OrganizationPersonalWorkspaceCreationException

    except NextcloudConversationCreationException:
        # TODO: add error logs
        _rollback_user_creation(user, superadmin_user)
        raise OrganizationPersonalWorkspaceCreationException

    except Exception as e:
        _rollback_user_creation(user, superadmin_user)
        # TODO: add error logs
        raise NextcloudGroupCreationException


def _rollback_user_creation(user, superadmin_user):
    nextcloud_user_manager.delete_user(
        user_id=user.username,
        auth_user_id=superadmin_user.get_nextcloud_user_id(),
        auth_user_app_password=superadmin_user.nc_app_password,
    )


def delete(team, auth_user):
    """
    Given a team and a team admin user to authenticate the request,
    wipes out the nextcloud group and removes the team.

    Parameters
    ----------
    team : Team
        The team model

    auth_user : User
        A user model. This user is assumed to have the required
        privileges in the team to perform nextcloud requests with an app password.

    Returns
    -------
        None

    Raises
    ------
    ValidationError
        If the group deletion fails.
    """
    try:
        nextcloud_group_manager.remove_group(
            group_id=team.nc_group_id,
            user=auth_user,
        )
    except Exception as e:
        # TODO: add logs
        raise OrganizationTeamDeletionException


def remove_from_team(user: User, team: Team, team_admin: User) -> None:
    """
    Given a user, a team and a admin team user to authenticate the request,
    removes the user from the given team.

    Parameters
    ----------
    user : User
        The user model

    team : Team
        The team model

    team_admin : User
        A user model. This user is assumed to have the required
        privileges in the team to perform nextcloud requests with an app password.

    Returns
    -------
        None

    Raises
    ------
    ValidationError
        If the user removal fails.
    """
    try:
        nextcloud_group_manager.remove_user_from_group(
            group_id=team.nc_group_id,
            user_id=user.get_nextcloud_user_id(),
            user=team_admin,
        )

        user_team = UserTeam.objects.get(user=user, team=team)
        user_team.delete()
    except Exception as e:
        logger.error(
            "Raised unexpected exception while removing user from team. Exception=%s", e
        )
        raise OrganizationRemoveTeamMemberException from e


def delete_personal_workspace(team, auth_user):
    """
    Given a personal workspace team and superadmin user to authenticate the request,
    removes the personal workspace with the given user.

    Parameters
    ----------
    team : Team
        The team model

    auth_user : User
        A user model. This user is assumed to have the required
        privileges to perform nextcloud requests with an app password.

    Returns
    -------
        None

    Raises
    ------
    ValidationError
        If the personal workspace deletion fails.
    """
    try:
        delete(team, auth_user)
    except Exception as e:
        raise ValidationError(
            ErrorSerializer("ERRORS_PERSONAL_WORKSPACE_DELETION_FAILED").data
        )


def get_list(
    auth_user: User,
    skip_personal_workspace: bool,
    last_message: bool,
    shared_files: bool,
) -> list[dict[str, str | int | dict]]:
    """
    Given a user to authenticate the request,
    list a user's teams, including member counts and shared fields for each one.

    Parameters
    ----------
    auth_user : User
        A user model. This user is assumed to have the required
        privileges to perform nextcloud requests with an app password.

    skip_personal_workspace : Bool
        Whether to add the personal workspace to the teams list.

    last_message : Bool
        Whether to add the team's latest message to the teams list.

    shared_files : Bool
        Whether to include the shared files count to the teams list.

    Returns
    -------
    list[dict[str, str | int | dict]]

    Raises
    ------
    ValidationError
        If the list teams fails.
    """
    try:
        teams = Team.objects.filter(userteam__user_id=auth_user.id, deleted=False)
        if skip_personal_workspace:
            teams = teams.filter(is_personal_workspace=False)

        teams_list = list(teams)
        team_serializer = TeamSerializer(teams_list, many=True)
        serialized_teams = team_serializer.data

        last_message_map = _get_last_message_by_conversation_map(auth_user)

        results = [
            {
                **team_data,
                "members_count": _get_team_members_count(
                    team := next(t for t in teams_list if t.id == team_data["id"])
                ),
                **(
                    {
                        "shared_files_count": _get_team_shared_files_count(
                            team, auth_user
                        )
                    }
                    if shared_files
                    else {}
                ),
                **(
                    {
                        "last_message": (
                            _get_last_message(last_message_map, team)
                            if last_message
                            else {}
                        )
                    }
                    if last_message
                    else {}
                ),
            }
            for team_data in serialized_teams
        ]

        return results

    except NextcloudUserConversationsFetchFailedException as e:
        logger.error("Unexpected error while fetching conversations. Exception=%s", e)
        raise OrganizationTeamListRetrievalFailedException


def get(
    team_id: int,
    auth_user: User,
    last_message: bool,
    shared_files: bool,
) -> dict[str, str | int | dict]:
    """
    Given a team_id and user to authenticate the request,
    returns a single team.

    Parameters
    ----------
    team_id : int
        The team id

    auth_user : User
        A user model. This user is assumed to have the required
        privileges to perform nextcloud requests with an app password.

    last_message : Bool
        Whether to add the team's latest message.

    shared_files : Bool
        Whether to add the shared files count.

    Returns
    -------
    dict[str, str | int | dict]

    Raises
    ------
    ValidationError
        If getting team data fails.
    """
    try:
        team: Team = Team.objects.filter(userteam__user_id=auth_user.id).get(id=team_id)

        return populate_team_data(
            team=team,
            auth_user=auth_user,
            shared_files=shared_files,
            last_message=last_message,
        )

    except Exception as e:
        raise ValidationError("ERRORS_GET_TEAM_FAILED")


def get_team_by_nc_group_id(team_name: str):
    return Team.objects.get(nc_group_id=team_name)


def populate_team_data(
    team: Team, auth_user: User, shared_files: bool, last_message: bool
):  # -> dict[Any | str, Any]:
    serialized_team_data = dict(TeamSerializer(team).data)

    last_message_map = _get_last_message_by_conversation_map(auth_user)

    team_data = {
        **serialized_team_data,
        "members_count": _get_team_members_count(team),
        **(
            {"shared_files_count": _get_team_shared_files_count(team, auth_user)}
            if shared_files
            else {}
        ),
        **(
            {"last_message": _get_last_message(last_message_map, team)}
            if last_message
            else {}
        ),
    }
    return team_data


def _get_team_members_count(team: Team) -> int:
    """
    Given a team, returns the count of team members.
    For personal workspaces always returns 1.

    Parameters
    ----------
    team : Team
        The team model

    Returns
    -------
    int
        The number of team members
    """
    if team.is_personal_workspace:
        return 1

    return UserTeam.objects.filter(team_id=team.id).count()


def _get_team_shared_files_count(team: Team, user: User) -> int:
    """
    Given a team and a user, returns the count of shared files in the team's conversation.

    Parameters
    ----------
    team : Team
        The team model

    user : User
        The user model to authenticate the request

    Returns
    -------
    int
        The number of shared files

    Raises
    ------
    ValidationError
        When getting shared files count fails.
    """
    try:
        from apps.nextcloud.resources import files as nextcloud_files_utils

        files = nextcloud_files_utils.list_conversation_files(
            conversation_token=team.nc_conversation_token,
            user=user,
        )
        return len(files["files"])
    except Exception:
        raise ValidationError("ERRORS_GET_SHARED_FILES_COUNT_FAILED")


def _get_last_message(last_message_map: dict[str, dict], team: Team) -> dict | None:
    """
    Get the last message for a team using its conversation token.

    Parameters
    ----------
    last_message_map : dict[str, dict]
        Mapping of conversation tokens to their last messages

    team : Team
        The team model instance

    Returns
    -------
    dict | None
        The last message data if found, None otherwise
    """
    return last_message_map.get(team.nc_conversation_token)


def _get_last_message_by_conversation_map(auth_user: User) -> dict[str, dict]:
    """
    Return a dictionary where each key is the conversation token id, and each value is the last message of the given conversation.

    Parameters
    ----------
    auth_user: User
        The authenticated user to get conversations for

    Returns
    -------
    dict[str, dict]
        Mapping of conversation tokens to their last messages
    """
    nc_conversations = nextcloud_talk_manager.get_user_conversations(user=auth_user)
    conversation_serializer = NextcloudLastConversationMessageSerializer(
        nc_conversations, many=True
    )
    serialized_conversations = conversation_serializer.data
    return {
        conversation["conversation_token"]: conversation["last_message"]
        for conversation in serialized_conversations
    }


def leave_team(user_team: UserTeam) -> None:
    """
    Given a `user_team` relationship, delegates to the nextcloud API to quit the
    conversation.
    """
    try:
        _check_leave_team_availability(user_team=user_team)
        nextcloud_group_manager.remove_user_from_group(
            group_id=user_team.team.nc_group_id,
            user_id=user_team.user.get_nextcloud_user_id(),
            user=get_super_admin(),
        )

    except (
        NextcloudLeaveConversationFailedException,
        NextcloudRemoveUserFromGroupFailedException,
    ) as e:
        logger.error(
            "A nextcloud exception was raised when leaving a team, Exception=%s", e
        )
        raise OrganizationLeaveTeamFailedException from e


def update_user_team_group(user_team: UserTeam, group: Group) -> UserTeam:
    """
    Given a `user_team` and a `group`, updates the relationship group.
    """
    match group:
        case Group(name="TeamOwner"):
            nextcloud_group_manager.promote_user_to_admin(
                user_id=user_team.user.get_nextcloud_user_id(), user=get_super_admin()
            )

        case Group(name="TeamAdmin"):
            if user_team.group.name == "TeamOwner":
                nextcloud_group_manager.demote_user_from_admin(
                    user_id=user_team.user.get_nextcloud_user_id(),
                    user=get_super_admin(),
                )
            nextcloud_group_manager.promote_user_to_subadmin(
                user_id=user_team.user.get_nextcloud_user_id(),
                group_id=user_team.team.nc_group_id,
                user=get_super_admin(),
            )

        case Group(name="TeamCollaborator"):
            if user_team.group.name == "TeamOwner":
                nextcloud_group_manager.demote_user_from_admin(
                    user_id=user_team.user.get_nextcloud_user_id(),
                    user=get_super_admin(),
                )

            if user_team.group.name == "TeamAdmin":
                nextcloud_group_manager.demote_user_from_subadmin(
                    user_id=user_team.user.get_nextcloud_user_id(),
                    group_id=user_team.team.nc_group_id,
                    user=get_super_admin(),
                )

    user_team.group = group
    user_team.save()
    return user_team


def _check_leave_team_availability(user_team: UserTeam) -> None:
    team: Team = user_team.team
    user_teams = UserTeam.objects.filter(team=team).exclude(user=user_team.user)
    if len(user_teams) == 0:
        from apps.organizations.organizations import delete_team

        delete_team(team)
    else:
        has_team_owner: bool = user_teams.filter(group__name="TeamOwner").exists()

        if not has_team_owner:
            some_user_team, *_user_teams = _list_team_members_by_group_hierarchy(
                team=team
            ).exclude(user=user_team.user)
            group: Group = Group.objects.get(name="TeamOwner")
            user_team: UserTeam = update_user_team_group(
                user_team=some_user_team, group=group
            )
            nextcloud_group_manager.promote_user_to_admin(
                user_id=user_team.user.get_nextcloud_user_id(), user=get_super_admin()
            )


def _list_team_members_by_group_hierarchy(team: Team) -> list[UserTeam]:
    group_order: dict[str, int] = {
        "TeamOwner": 1,
        "TeamAdmin": 2,
        "TeamCollaborator": 3,
    }
    return (
        UserTeam.objects.filter(team=team)
        .select_related("user", "group")
        .annotate(
            group_order=Case(
                When(group__name="TeamOwner", then=Value(group_order["TeamOwner"])),
                When(group__name="TeamAdmin", then=Value(group_order["TeamAdmin"])),
                When(
                    group__name="TeamCollaborator",
                    then=Value(group_order["TeamCollaborator"]),
                ),
                default=Value(4),  # Default order for any other groups
                output_field=IntegerField(),
            )
        )
        .order_by("group_order")
    )
