from django.contrib.auth.models import (
    Group,
)
from drf_extra_fields.fields import Base64ImageField
from drf_spectacular.utils import extend_schema_field
from rest_framework import serializers

from apps.accounts.models import Language, User
from apps.organizations.models import (
    ExternalHyperlink,
    FavouriteTeam,
    Organization,
    Team,
    UserOrganizationGroup,
)
from colmena.validators import files as files_validator


class CountrySerializer(serializers.Serializer):
    name = serializers.CharField()
    iso_code = serializers.CharField(source="code", required=False)


# ------------------------------------------------------------------------------
# Organization serializers
#


class OrganizationLanguageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Language
        fields = "__all__"


class OrganizationMemberGroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = (
            "id",
            "name",
        )


class OrganizationMemberSerializer(serializers.ModelSerializer):
    avatar = Base64ImageField(required=False, represent_in_base64=True)

    class Meta:
        model = User
        fields = (
            "pk",
            "username",
            "full_name",
            "avatar",
            "email",
        )


class UserOrganizationGroupSerializer(serializers.ModelSerializer):
    group = OrganizationMemberGroupSerializer()
    user = OrganizationMemberSerializer()

    class Meta:
        model = UserOrganizationGroup
        fields = (
            "group",
            "user",
            "created_at",
            "updated_at",
        )

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation["user"] = OrganizationMemberSerializer(instance.user).data
        representation["group"] = OrganizationMemberGroupSerializer(instance.group).data
        return representation


class OrganizationSerializer(serializers.ModelSerializer):
    logo = Base64ImageField(required=False, represent_in_base64=True)

    class Meta:
        model = Organization
        fields = (
            "id",
            "name",
            "email",
            "website",
            "country",
            "logo",
            "additional_info",
            "created_by",
            "language",
        )
        extra_kwargs = {
            "website": {"error_messages": {"invalid": "ERRORS_INVALID_WEBSITE_FORMAT"}},
            "country": {"error_messages": {"invalid_choice": "ERRORS_INVALID_COUNTRY"}},
            "language": {
                "error_messages": {"does_not_exist": "ERRORS_INVALID_LANGUAGE"}
            },
        }

    def validate_logo(self, value):
        if value:
            files_validator.validate_max_size(value)
            return value

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation["country"] = CountrySerializer(instance.country).data
        representation["language"] = OrganizationLanguageSerializer(
            instance.language
        ).data
        return representation


class OrganizationMembersRequestSerializer(serializers.Serializer):
    organization_id = serializers.IntegerField(required=True)


class OrganizationMembersDataSerializer(serializers.Serializer):
    users = UserOrganizationGroupSerializer(many=True)
    amount = serializers.SerializerMethodField()

    @extend_schema_field(serializers.IntegerField)
    def get_amount(self, instance):
        return len(instance["users"])

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation["amount"] = self.get_amount(instance)
        return representation


class OrganizationMembersResponseSerializer(serializers.Serializer):
    active = OrganizationMembersDataSerializer()
    suspended = OrganizationMembersDataSerializer()


# ------------------------------------------------------------------------------
# Team serializers
#


class PermissionGroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = ["id", "name"]


class TeamSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()
    organization_id = serializers.PrimaryKeyRelatedField(
        read_only=True, allow_null=True
    )
    organization_name = serializers.CharField(
        source="organization.name", allow_null=True
    )
    logo = Base64ImageField(required=False, represent_in_base64=True)

    class Meta:
        model = Team
        fields = (
            "id",
            "name",
            "description",
            "nc_group_id",
            "nc_conversation_token",
            "organization_id",
            "organization_name",
            "is_personal_workspace",
            "logo",
        )


class TeamCreateUsersRequestSerializer(serializers.Serializer):
    user_id = serializers.IntegerField(required=True)


class AddTeamMemberRequestSerializer(serializers.Serializer):
    organization_id = serializers.IntegerField(required=True)
    team_id = serializers.IntegerField(required=True)
    users = serializers.ListField(
        child=serializers.IntegerField(), required=False, allow_empty=True, default=[]
    )


class AddTeamMemberResponseSerializer(serializers.Serializer):
    result = serializers.CharField(required=True)


class RemoveTeamMemberRequestSerializer(serializers.Serializer):
    team_id = serializers.IntegerField(required=True)
    user_id = serializers.IntegerField(required=True)


class ListTeamMembersRequestSerializer(serializers.Serializer):
    include_myself = serializers.BooleanField(required=True)
    team_id = serializers.IntegerField(required=True)


class GetTeamMemberRequestSerializer(serializers.Serializer):
    team_id = serializers.IntegerField(required=True)
    member_id = serializers.IntegerField(required=True)


class TeamUserSerialzier(serializers.ModelSerializer):
    avatar = Base64ImageField(required=False, represent_in_base64=True)

    class Meta:
        model = User
        fields = ["id", "username", "full_name", "email", "avatar"]


class TeamMemberSerializer(serializers.Serializer):
    user = TeamUserSerialzier(required=True)
    group = PermissionGroupSerializer(required=True)


class ListTeamMembersResponseSerializer(serializers.Serializer):
    members = serializers.ListField(child=TeamMemberSerializer(), required=True)


class RemoveTeamMemberResponseSerializer(serializers.Serializer):
    result = serializers.CharField(required=True)


class LeaveTeamRequestSerializer(serializers.Serializer):
    team_id = serializers.IntegerField(required=True)


class LeaveTeamResponseSerializer(serializers.Serializer):
    result = serializers.CharField(required=True)


class ListTeamGroupsResponseSerializer(serializers.Serializer):
    groups = serializers.ListField(child=PermissionGroupSerializer(), required=True)


class ListTeamsByUserRequestSerializer(serializers.Serializer):
    member_id = serializers.IntegerField(required=True)


class MemberTeamsSerializer(serializers.Serializer):
    team = TeamSerializer(required=True)
    group = PermissionGroupSerializer(required=True)


class ListTeamsByUserResponseSerializer(serializers.Serializer):
    user_team_groups = serializers.ListField(
        child=MemberTeamsSerializer(), required=True
    )


class TeamCreateRequestSerializer(serializers.Serializer):
    organization_id = serializers.IntegerField(required=True)
    name = serializers.CharField(required=True, max_length=255)
    description = serializers.CharField(required=False, max_length=1000)
    logo = Base64ImageField(required=False, represent_in_base64=True)
    users = serializers.ListField(
        child=serializers.IntegerField(), required=False, allow_empty=True, default=[]
    )
    new_org_users = serializers.ListField(
        child=serializers.IntegerField(), required=False, allow_empty=True, default=[]
    )

    def validate_logo(self, value):
        if value:
            files_validator.validate_max_size(value)
        return value


class TeamRequestSerializer(serializers.Serializer):
    last_message = serializers.BooleanField(required=False, default=False)
    shared_files = serializers.BooleanField(required=False, default=False)

    def get_object(self):
        obj = super().get_object()
        if obj.deleted:
            raise serializers.ValidationError("ERRORS_DELETED_TEAM")
        return obj


class TeamPartialUpdateSerializer(serializers.ModelSerializer):
    """
    Represents the request serializer class to partially update a team's details.
    """

    logo = Base64ImageField(required=False)

    class Meta:
        model = Team
        fields = ("name", "description", "logo")

    def validate_logo(self, value):
        instance = self.instance
        if instance and instance.logo and instance.logo == value:
            return instance.logo
        return value


class TeamListRequestSerializer(TeamRequestSerializer):
    skip_personal_workspace = serializers.BooleanField(required=False, default=False)
    shared_files = serializers.BooleanField(required=False, default=False)


class TeamResponseSerializer(TeamSerializer):
    organization_id = serializers.IntegerField(allow_null=True)
    last_message = serializers.DictField(required=False)
    organization_name = serializers.SerializerMethodField()
    members_count = serializers.IntegerField()
    shared_files_count = serializers.IntegerField(required=False)
    logo = serializers.CharField(required=False)
    description = serializers.CharField(required=False)

    class Meta(TeamSerializer.Meta):
        fields = TeamSerializer.Meta.fields + (
            "last_message",
            "members_count",
            "shared_files_count",
            "logo",
            "description",
        )

    @extend_schema_field(serializers.CharField())
    def get_organization_name(self, obj) -> str:
        return obj.get("organization_name")


class TeamListSharedFilesResponseSerializer(serializers.Serializer):
    files = serializers.ListField(child=serializers.DictField())

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation["quantity"] = len(representation["files"])
        return representation


class UpdateUserTeamPermissionsRequestSerializer(serializers.Serializer):
    """
    Represents the request serializer for updating user's permissions in a team.
    """

    team_id = serializers.IntegerField(required=True)
    user_id = serializers.IntegerField(required=True)
    group_id = serializers.IntegerField(required=True)


class UpdateUserTeamPermissionsResponseSerializer(serializers.Serializer):
    """
    Represents the response serializer when updating user's permissions in a
    team.
    """

    result = serializers.CharField(required=True)
    group = PermissionGroupSerializer()


# ------------------------------------------------------------------------------
# Share serializers
#


class ShareUploadRequestSerializer(serializers.Serializer):
    file = serializers.FileField(required=True)
    filename = serializers.CharField(required=True)
    teams = serializers.CharField()

    def validate_filename(self, value):
        _, extension = files_validator.get_filename_extension(value)
        if files_validator.is_valid_extension(extension):
            return value
        else:
            raise serializers.ValidationError("ERRORS_EXTENSION_IS_NOT_VALID")

    def validate_teams(self, value):
        try:
            teams_ids = value.split(",")
            teams = Team.objects.filter(
                userteam__user_id=self.context.get("user_id"),
                userteam__team_id__in=teams_ids,
            )
            assert len(teams_ids) == teams.count()
            return teams
        except Exception as e:
            raise serializers.ValidationError("ERRORS_TEAMS_SELECTION_IS_NOT_VALID")


class ShareProjectRequestSerializer(ShareUploadRequestSerializer):
    def validate_filename(self, value):
        _, extension = files_validator.get_filename_extension(value)
        if files_validator.is_project_extension(extension):
            return value
        else:
            raise serializers.ValidationError("ERRORS_EXTENSION_IS_NOT_VALID")


class ProjectRequestSerializer(serializers.Serializer):
    pass


class ProjectImportRequestSerializer(serializers.Serializer):
    project_id = serializers.IntegerField(required=True)


class ProjectSharedTeamSerializer(serializers.Serializer):
    team_id = serializers.IntegerField(required=True)
    name = serializers.CharField()
    is_personal_workspace = serializers.BooleanField()


class ProjectResponseSerializer(serializers.Serializer):
    id = serializers.IntegerField(required=True)
    name = serializers.CharField()
    shared_teams = ProjectSharedTeamSerializer(many=True)


# ------------------------------------------------------------------------------
# External Hyperlinks serializers
#


class ExternalHyperlinkSerializer(serializers.ModelSerializer):
    logo = Base64ImageField(required=True, represent_in_base64=True)
    organization_id = serializers.IntegerField(required=True)
    id = serializers.IntegerField(read_only=True)

    class Meta:
        model = ExternalHyperlink
        fields = ("name", "url", "logo", "organization_id", "id")
        extra_kwargs = {"id": {"required": False}}


class ExternalHyperlinkCreateSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=255)
    url = serializers.URLField(max_length=200)
    logo = Base64ImageField(required=True, represent_in_base64=True)
    organization_id = serializers.IntegerField(required=True)

    def create(self, validated_data):
        return ExternalHyperlink.objects.create(**validated_data)


class FavouriteTeamSerializer(serializers.ModelSerializer):
    class Meta:
        model = FavouriteTeam
        fields = "__all__"

    def create(self, validated_data):
        return FavouriteTeam.objects.create(**validated_data)


class FavouriteTeamCreateSerializer(serializers.Serializer):
    team_id = serializers.IntegerField(required=True)


class FavouriteTeamResponseSerializer(serializers.ModelSerializer):
    team_name = serializers.CharField(source="team.name", read_only=True)
    team_logo = Base64ImageField(
        source="team.logo", read_only=True, represent_in_base64=True
    )

    class Meta:
        model = FavouriteTeam
        fields = ["id", "team", "team_name", "team_logo"]
