from apps.organizations.models import Organization
from apps.accounts.support import user_fixture, language_fixture

from colmena.support.test_utils import unique_value


VALID_ATTRS = {
    "name": "some organization name",
    "email": "some@email.net",
    "website": "http://www.website.net",
    "country": "AR",
    "additional_info": "aditional info",
    "logo": "",
}

UPDATE_ATTRS = {
    "name": "some updated organization name",
    "email": "some:updated@email.net",
    "website": "http://www.websiteupdated.net",
    "country": "ES",
    "additional_info": "updated aditional info",
    "logo": "",
}

INVALID_ATTRS = {
    "name": None,
    "email": None,
    "website": None,
    "country": None,
    "additional_info": None,
    "language": None,
    "logo": "invalid logo",
}


def valid_attrs(attrs={}):
    return VALID_ATTRS | {"name": unique_value(VALID_ATTRS["name"])} | attrs


def update_attrs(attrs={}):
    return UPDATE_ATTRS | {"name": unique_value(UPDATE_ATTRS["name"])} | attrs


def invalid_attrs(attrs={}):
    return INVALID_ATTRS | attrs


def create(attrs={}):
    attrs = maybe_assign_user(attrs)
    attrs = maybe_assign_language(attrs)
    return Organization.objects.create(**valid_attrs(attrs))


def maybe_assign_user(attrs):
    key = "created_by"
    if attrs.get(key) is None:
        attrs[key] = user_fixture.create(attrs.get("user_attrs", {}))
    return attrs


def maybe_assign_language(attrs):
    key = "language"
    if attrs.get(key) is None:
        attrs[key] = language_fixture.create(attrs.get("language_attrs", {}))
    return attrs
