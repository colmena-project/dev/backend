from django.urls import path

from . import views


urlpatterns = [
    path(
        "shares/<filename>/upload",
        views.ShareUploadAPIView.as_view(),
        name="share_upload",
    ),
    path(
        "projects/<str:filename>/export",
        views.ProjectExportAPIView().as_view(),
        name="export_project",
    ),
    path(
        "projects/",
        views.ProjectListAPIView().as_view(),
        name="list_projects",
    ),
    path(
        "projects/<int:project_id>/import",
        views.ProjectImportAPIView().as_view(),
        name="import_project",
    ),
]
