"""
Main views module for the organizations app.
"""

import logging

from constance import config

from django.db.models import Q
from django.contrib.auth.models import Group
from django.http import HttpResponse

from django_countries import countries

from drf_spectacular.types import OpenApiTypes
from drf_spectacular.utils import extend_schema, OpenApiParameter, OpenApiExample

from rest_framework import permissions, viewsets, mixins
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework.parsers import MultiPartParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView

from rest_framework_simplejwt.authentication import JWTAuthentication

from apps.invitations.exceptions import UnauthorizedUserException
from apps.nextcloud.serializers import NextcloudListSharedFilesRequestSerializer
from apps.nextcloud.resources import files as nextcloud_files_utils
from apps.nextcloud.resources.utils import create_file_name

from apps.organizations import organizations
from apps.organizations.models import (
    ExternalHyperlink,
    FavouriteTeam,
    Organization,
    Team,
    UserOrganizationGroup,
    UserOrganizationGroupProxy,
    UserTeam,
)
from apps.organizations.exceptions import (
    OrganizationRemoveTeamMemberException,
    OrganizationShareUploadedFileException,
    OrganizationProjectsRetrievalException,
    OrganizationProjectNotFoundException,
    OrganizationProjectDownloadException,
    OrganizationTeamListRetrievalFailedException,
    OrganizationTeamCreationException,
    OrganizationAddTeamMemberException,
    OrganizationSingleTeamRetrievalFailedException,
    OrganizationLeaveTeamFailedException,
    OrganizationUpdateUserTeamPermissionsFailedException,
    ListTeamMembersFailedException,
    GetTeamMemberFailedException,
)
from apps.organizations.permissions import (
    BelongsToOrganization,
    CanCreateTeam,
    CanDeleteTeam,
    CanInviteMembers,
    CanLeaveTeam,
    CanListTeamMembers,
    CanRemoveMember,
    CanRetrieveTeamMember,
    CanUpdateTeam,
    CanUpdateUserTeamPermissions,
    IsAdminOrOrgOwner,
)
from apps.organizations.resources import team as organization_team_manager
from apps.organizations.resources import shares as organization_share_manager
from apps.organizations.serializers import (
    AddTeamMemberRequestSerializer,
    AddTeamMemberResponseSerializer,
    CountrySerializer,
    ExternalHyperlinkCreateSerializer,
    ExternalHyperlinkSerializer,
    FavouriteTeamCreateSerializer,
    FavouriteTeamResponseSerializer,
    FavouriteTeamSerializer,
    GetTeamMemberRequestSerializer,
    LeaveTeamRequestSerializer,
    LeaveTeamResponseSerializer,
    ListTeamGroupsResponseSerializer,
    ListTeamMembersRequestSerializer,
    ListTeamMembersResponseSerializer,
    OrganizationMembersRequestSerializer,
    OrganizationMembersResponseSerializer,
    OrganizationSerializer,
    ProjectImportRequestSerializer,
    ProjectRequestSerializer,
    ProjectResponseSerializer,
    RemoveTeamMemberRequestSerializer,
    RemoveTeamMemberResponseSerializer,
    ShareProjectRequestSerializer,
    ShareUploadRequestSerializer,
    TeamCreateRequestSerializer,
    TeamListRequestSerializer,
    TeamListSharedFilesResponseSerializer,
    TeamMemberSerializer,
    TeamPartialUpdateSerializer,
    TeamRequestSerializer,
    TeamResponseSerializer,
    TeamSerializer,
    UpdateUserTeamPermissionsRequestSerializer,
    UpdateUserTeamPermissionsResponseSerializer,
    ListTeamsByUserRequestSerializer,
    ListTeamsByUserResponseSerializer,
)

from colmena.serializers.serializers import ErrorSerializer
from colmena.utils import encoding

logger = logging.getLogger(__name__)


class CountryViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = CountrySerializer
    permission_classes = [permissions.IsAuthenticated]
    queryset = [
        {"name": value, "code": key} for key, value in countries.countries.items()
    ]


class OrganizationViewSet(viewsets.ModelViewSet):
    queryset = organizations.list_organizations()
    serializer_class = OrganizationSerializer
    permission_classes = [permissions.IsAuthenticated]
    http_method_names = ["get", "patch"]

    def partial_update(self, request, *args, **kwargs):
        user_request = request.user
        organization_id = kwargs.get("pk")
        if not Organization.is_valid_pk(organization_id):
            return Response(ErrorSerializer("ERRORS_INVALID_REQUEST").data, status=404)
        # FIXME : Create a function in model to check this condition
        if not UserOrganizationGroupProxy.objects.filter(
            Q(
                user=user_request,
                organization=organization_id,
                group=Group.objects.get(name="OrgOwner"),
            )
            | Q(
                user=user_request,
                organization=organization_id,
                group=Group.objects.get(name="Admin"),
            )
        ).exists():
            return Response(
                ErrorSerializer("ERRORS_INVALID_PERMISSION").data, status=401
            )
        return super().partial_update(request)

    def retrieve(self, request, *args, **kwargs):
        organization = self.get_object()
        user_request = request.user
        user_organization_group = UserOrganizationGroupProxy.objects.filter(
            organization=organization
        )
        if not user_organization_group.filter(user=user_request).exists():
            return Response(
                ErrorSerializer("ERRORS_USER_IS_NOT_ORGANIZATION_MEMBER").data,
                status=403,
            )
        serializer = self.get_serializer(organization)
        return Response(serializer.data)

    @action(detail=False)
    def byuser(self, request):
        user_request = request.user
        user_organization_group = UserOrganizationGroupProxy.objects.filter(
            user=user_request
        )
        if not user_organization_group.exists():
            return Response(
                ErrorSerializer(
                    "ERRORS_NO_ORGANIZATION_FOUND_FOR_THE_CURRENT_USER"
                ).data,
                status=404,
            )

        serializer = self.get_serializer(user_organization_group.first().organization)
        return Response(serializer.data, status=200)


class TeamDestroyView(mixins.DestroyModelMixin):
    def perform_destroy(self, instance):
        """
        Instead of permanently deleting the team, set its `deleted` attribute to True.
        """
        organizations.delete_team(instance)


class TeamViewSet(
    mixins.RetrieveModelMixin,
    mixins.ListModelMixin,
    viewsets.GenericViewSet,
    mixins.UpdateModelMixin,
    TeamDestroyView,
):
    """
    Viewset that defines entrypoints to access and modify team entities.
    """

    queryset = Team.objects.filter(deleted=False)
    serializer_class = TeamSerializer
    permission_classes = [
        permissions.IsAuthenticated,
        CanCreateTeam,
        CanInviteMembers,
        CanUpdateTeam,
        CanRemoveMember,
        CanLeaveTeam,
        CanUpdateUserTeamPermissions,
        CanListTeamMembers,
        CanRetrieveTeamMember,
        CanDeleteTeam,
    ]
    http_method_names = ["post", "get", "patch", "delete"]

    def get_serializer_class(self):
        if self.action == "partial_update":
            return TeamPartialUpdateSerializer
        return super().get_serializer_class()

    @extend_schema(
        responses=TeamResponseSerializer(many=True),
        parameters=[
            OpenApiParameter(
                name="skip_personal_workspace",
                description="Whether to return the personal workspace team or not.",
                type=bool,
                required=False,
            ),
            OpenApiParameter(
                name="last_message",
                description="Whether to add the team's latest message to the list or not.",
                type=bool,
                required=False,
            ),
            OpenApiParameter(
                name="shared_files",
                description="Whether to add the shared files count to the list or not.",
                type=bool,
                required=False,
            ),
        ],
        description="Returns all teams for the given user",
        examples=[
            OpenApiExample(
                "Example 1",
                summary="Returns Teams list",
                value=[
                    {
                        "id": 7,
                        "nc_group_id": "organization-2-general",
                        "nc_conversation_token": "yxagfqh6",
                        "organization_id": 2,
                        "organization_name": "The Organization",
                        "is_personal_workspace": False,
                        "members_count": 2,
                    },
                    {
                        "id": 8,
                        "nc_group_id": "user-personal-workspace",
                        "nc_conversation_token": "ek6o6pes",
                        "organization_id": None,
                        "is_personal_workspace": True,
                        "members_count": 2,
                    },
                ],
            ),
            OpenApiExample(
                "Example 2",
                summary="Returns Teams list with shared_files_count and last_message for each team",
                value=[
                    {
                        "id": 7,
                        "nc_group_id": "organization-2-general",
                        "nc_conversation_token": "yxagfqh6",
                        "organization_id": 2,
                        "organization_name": "The Organization",
                        "is_personal_workspace": False,
                        "last_message": {
                            "last_message_id": 174,
                            "last_message_text": "{actor} removed group {group}",
                            "last_message_timestamp": 1701106637,
                            "last_messages_parameters": {
                                "actor": {"type": "user", "id": "user", "name": "user"},
                                "group": {
                                    "type": "group",
                                    "id": "user-personal-workspace",
                                    "name": "user-personal-workspace",
                                },
                            },
                        },
                        "members_count": 2,
                        "shared_files_count": 5,
                    },
                    {
                        "id": 8,
                        "nc_group_id": "user-personal-workspace",
                        "nc_conversation_token": "ek6o6pes",
                        "organization_id": None,
                        "is_personal_workspace": True,
                        "last_message": {
                            "last_message_id": 1403,
                            "last_message_text": "Hello World",
                            "last_message_timestamp": 1701187609,
                            "last_messages_parameters": {},
                        },
                        "members_count": 2,
                        "shared_files_count": 2,
                    },
                ],
            ),
            OpenApiExample(
                "Example 3",
                summary="Returns Teams list with shared_files_count and last_message for each team and skips personal workspace",
                value=[
                    {
                        "id": 7,
                        "nc_group_id": "organization-2-general",
                        "nc_conversation_token": "yxagfqh6",
                        "organization_id": 2,
                        "organization_name": "The Organization",
                        "is_personal_workspace": False,
                        "last_message": {
                            "last_message_id": 174,
                            "last_message_text": "{actor} removed group {group}",
                            "last_message_timestamp": 1701106637,
                            "last_messages_parameters": {
                                "actor": {"type": "user", "id": "user", "name": "user"},
                                "group": {
                                    "type": "group",
                                    "id": "user-personal-workspace",
                                    "name": "user-personal-workspace",
                                },
                            },
                        },
                        "members_count": 2,
                        "shared_files_count": 5,
                    },
                ],
            ),
        ],
    )
    def list(self, request, *args, **kwargs):
        try:
            request_serializer = TeamListRequestSerializer(data=request.query_params)
            request_serializer.is_valid(raise_exception=True)
            teams = organization_team_manager.get_list(
                request.user,
                request_serializer.data.get("skip_personal_workspace"),
                request_serializer.data.get("last_message"),
                request_serializer.data.get("shared_files"),
            )
            serializer = TeamResponseSerializer(teams, many=True)
            return Response(serializer.data, status=200)

        except OrganizationTeamListRetrievalFailedException:
            return Response(
                ErrorSerializer("ERRORS_TEAM_LIST_RETRIEVAL_FAILED").data,
                status=400,
            )

    @extend_schema(
        responses=TeamResponseSerializer(),
        parameters=[
            OpenApiParameter(
                name="last_message",
                description="Whether to add the team's latest message or not.",
                type=bool,
                required=False,
            ),
            OpenApiParameter(
                name="shared_files",
                description="Whether to add the shared files count or not.",
                type=bool,
                required=False,
            ),
        ],
        description="Returns a single team for the given user",
        examples=[
            OpenApiExample(
                "Example 1",
                summary="Returns a single team",
                value={
                    "id": 7,
                    "nc_group_id": "organization-2-general",
                    "nc_conversation_token": "yxagfqh6",
                    "organization_id": 2,
                    "organization_name": "The Organization",
                    "is_personal_workspace": False,
                    "members_count": 2,
                },
            ),
            OpenApiExample(
                "Example 2",
                summary="Returns a single team with shared_files_count and last_message",
                value={
                    "id": 7,
                    "nc_group_id": "organization-2-general",
                    "nc_conversation_token": "yxagfqh6",
                    "organization_id": 2,
                    "organization_name": "The Organization",
                    "is_personal_workspace": False,
                    "last_message": {
                        "last_message_id": 174,
                        "last_message_text": "{actor} removed group {group}",
                        "last_message_timestamp": 1701106637,
                        "last_messages_parameters": {
                            "actor": {"type": "user", "id": "user", "name": "user"},
                            "group": {
                                "type": "group",
                                "id": "user-personal-workspace",
                                "name": "user-personal-workspace",
                            },
                        },
                    },
                    "members_count": 2,
                    "shared_files_count": 5,
                },
            ),
        ],
    )
    def retrieve(self, request, *args, **kwargs):
        try:
            team = self.get_object()
            request_serializer = TeamRequestSerializer(data=request.query_params)
            if request_serializer.is_valid(raise_exception=True):
                team_data = organization_team_manager.get(
                    team.id,
                    request.user,
                    request_serializer.data.get("last_message"),
                    request_serializer.data.get("shared_files"),
                )

                serializer = TeamResponseSerializer(team_data)
                return Response(serializer.data, status=200)
        except Exception:
            return Response(
                ErrorSerializer("ERRORS_TEAM_NOT_FOUND").data,
                status=404,
            )

    @extend_schema(
        responses=NextcloudListSharedFilesRequestSerializer,
        parameters=[
            OpenApiParameter(
                name="pk",
                description="The team id",
                location=OpenApiParameter.PATH,
                type=str,
                required=True,
                examples=[
                    OpenApiExample("Team with ID 2", value="2"),
                    OpenApiExample("Team with ID 3", value="3"),
                ],
            ),
            OpenApiParameter(
                name="limit",
                description="The max number of files to return.",
                type=int,
                required=False,
                default=200,
            ),
            OpenApiParameter(
                name="descending",
                description="Whether to return items in ascending or descending order.",
                type=bool,
                required=False,
                default=True,
            ),
        ],
        description="Returns all files and media shared for the given team",
        examples=[
            OpenApiExample(
                "Example 1",
                summary="Returns a list of team details",
                value={
                    "files": [
                        {
                            "id": 1112,
                            "token": "3px44s3e",
                            "actorType": "users",
                            "actorId": "test",
                            "actorDisplayName": "test",
                            "timestamp": 1698941776,
                            "message": "{file}",
                            "messageParameters": {
                                "actor": {
                                    "type": "user",
                                    "id": "test",
                                    "name": "test",
                                },
                                "file": {
                                    "type": "file",
                                    "id": "1507",
                                    "name": "test.png",
                                    "size": 265370,
                                    "path": "test.png",
                                    "link": "https://some-url/f/1507",
                                    "etag": "7675a9eb16e72ff51ec0e9c182ec6b57",
                                    "permissions": 27,
                                    "mimetype": "image/png",
                                    "preview-available": "yes",
                                },
                            },
                            "systemMessage": "",
                            "messageType": "comment",
                            "isReplyable": True,
                            "referenceId": "2ac1ea706cdbf08f4bc48c1644575b8807e4ba7630002cec1713feab1b27d214",
                            "reactions": {},
                            "expirationTimestamp": 0,
                            "markdown": True,
                        }
                    ],
                    "quantity": 1,
                },
            )
        ],
    )
    @action(detail=False, url_path="(?P<pk>[^/.]+)/shared_files")
    def list_shared_files(self, request, *args, **kwargs):
        """
        Returns a list of shared files for a given team.
        """
        team = self.get_object()
        data = {
            "conversation_token": team.nc_conversation_token,
        }

        if request.query_params.get("limit", False):
            data = data | {"limit": request.query_params.get("limit")}

        if request.query_params.get("descending", False):
            data = data | {"descending": request.query_params.get("descending")}

        request_serializer = NextcloudListSharedFilesRequestSerializer(
            data=data,
        )

        if not request_serializer.is_valid():
            # TODO: Find a way to send the serializer error dictionary, in addition to the error message.
            return Response(
                ErrorSerializer("ERRORS_FILES_LIST_INVALID_REQUEST_PARAMS").data,
                status=400,
            )

        args = request_serializer.data | {"user": request.user}

        try:
            response = nextcloud_files_utils.list_conversation_files(**args)
            serializer = TeamListSharedFilesResponseSerializer(response)
            return Response(serializer.data)
        except Exception:
            return Response(
                ErrorSerializer("ERRORS_NEXTCLOUD_SERVER_ERROR").data, status=404
            )

    @extend_schema(
        request=TeamCreateRequestSerializer,
        responses={
            201: TeamResponseSerializer,
            400: {"error_code": "ERRORS_TEAM_CREATION_FAILED"},
            400: {"error_code": "ERRORS_UNEXPECTED_ERROR_ON_TEAM_CREATION"},
            403: {"detail": "You do not have permission to perform this action."},
        },
        description="Creates a new team in the user's organization",
        examples=[
            OpenApiExample(
                "Example 1",
                summary="Team created successfully",
                value={
                    "id": 7,
                    "name": "The Team",
                    "nc_group_id": "organization-2-general",
                    "nc_conversation_token": "yxagfqh6",
                    "organization_id": 2,
                    "organization_name": "The Organization",
                    "is_personal_workspace": False,
                    "members_count": 2,
                    "shared_files_count": 0,
                },
            ),
            OpenApiExample(
                "Example 2",
                summary="Team creation failed",
                value={"error_code": "ERROR_TEAM_CREATION_FAILED"},
            ),
            OpenApiExample(
                "Example 3",
                summary="Unauthorized to create a Team",
                value={"detail": "You do not have permission to perform this action."},
            ),
        ],
    )
    def create(self, request, *args, **kwargs):
        serializer = TeamCreateRequestSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        try:
            team_data = organizations.create_team(
                user=request.user,
                organization_id=serializer.validated_data["organization_id"],
                name=serializer.validated_data["name"],
                description=serializer.validated_data["description"],
                logo=serializer.validated_data["logo"],
                users=serializer.validated_data["users"],
                new_users=serializer.validated_data["new_org_users"],
            )

            # Get the base team data
            serialized_team_data = TeamResponseSerializer(team_data).data

            # Create final response using TeamResponseSerializer
            return Response(serialized_team_data, status=201)

        except OrganizationTeamCreationException:
            return Response(
                ErrorSerializer("ERRORS_TEAM_CREATION_FAILED").data,
                status=400,
            )

        except Exception as e:
            logger.error("Raised unexpected exception on team creation Exception=%s", e)
            return Response(
                ErrorSerializer("ERRORS_UNEXPECTED_ERROR_ON_TEAM_CREATION").data,
                status=400,
            )

    @extend_schema(
        parameters=[
            OpenApiParameter(
                name="team_id",
                description="The team id",
                location=OpenApiParameter.PATH,
                type=str,
                required=True,
                examples=[
                    OpenApiExample("Team id example", value="53"),
                ],
            ),
            OpenApiParameter(
                name="include_myself",
                description="Should include the current user in the final response",
                location=OpenApiParameter.QUERY,
                type=int,
                default=1,
                examples=[
                    OpenApiExample("Include myself", value=1),
                ],
            ),
        ],
        request=ListTeamMembersRequestSerializer,
        responses={
            200: ListTeamMembersResponseSerializer,
            400: {"error_code": "ERRORS_LIST_TEAM_MEMBERS_FAILED"},
            500: {"error_code": "ERRORS_UNEXPECTED_ERROR_ON_LIST_TEAM_MEMBERS"},
            403: {"detail": "You do not have permission to perform this action."},
        },
        description="Lists members of a team",
        examples=[
            OpenApiExample(
                "List members successfully",
                summary="Members are listed successfully",
                value=[
                    {
                        "members": [
                            {
                                "user": {
                                    "id": 13,
                                    "username": "johnny",
                                    "full_name": "John paul",
                                    "email": "john@company.com",
                                },
                                "group": {"id": 6, "name": "TeamOwner"},
                            },
                            {
                                "user": {
                                    "id": 14,
                                    "username": "charles",
                                    "full_name": "Charlie parker",
                                    "email": "charliep@company.com",
                                },
                                "group": {"id": 8, "name": "TeamCollaborator"},
                            },
                        ]
                    }
                ],
            ),
            OpenApiExample(
                "Request with domain error",
                summary="Team members retrieval failed",
                value={"error_code": "ERRORS_LIST_TEAM_MEMBERS_FAILED"},
            ),
            OpenApiExample(
                "Request with low permissions",
                summary="Unauthorized to list team members",
                value={"detail": "You do not have permission to perform this action."},
            ),
        ],
    )
    @action(detail=False, url_path="(?P<team_id>[^/.]+)/members", methods=["get"])
    def list_members(self, request, *args, **kwargs) -> Response:
        """
        Given a `team_id` returns a list of members.
        """
        serializer = ListTeamMembersRequestSerializer(
            data={
                **request.data,
                **kwargs,
                **{"include_myself": request.query_params.get("include_myself", 1)},
            }
        )
        serializer.is_valid(raise_exception=True)

        try:
            # List members
            exclude_users = (
                [] if serializer.validated_data["include_myself"] else [request.user]
            )
            team_members = organizations.list_team_members(
                team_id=serializer.validated_data["team_id"],
                exclude_users=exclude_users,
            )

            serialized_team_data = ListTeamMembersResponseSerializer(
                {"members": team_members}
            ).data

            return Response(serialized_team_data, status=200)

        except ListTeamMembersFailedException:
            return Response(
                ErrorSerializer("ERRORS_LIST_TEAM_MEMBERS_FAILED").data,
                status=400,
            )

        except Exception as e:
            logger.error("Raised unexpected exception on team creation Exception=%s", e)
            return Response(
                ErrorSerializer("ERRORS_UNEXPECTED_ERROR_ON_LIST_TEAM_MEMBERS").data,
                status=500,
            )

    @extend_schema(
        parameters=[
            OpenApiParameter(
                name="team_id",
                description="The team id",
                location=OpenApiParameter.PATH,
                type=str,
                required=True,
                examples=[
                    OpenApiExample("Team id example", value="53"),
                ],
            ),
        ],
        request=AddTeamMemberRequestSerializer,
        responses={
            200: AddTeamMemberResponseSerializer,
            400: {"error_code": "ERRORS_ADD_TEAM_MEMBERS_FAILED"},
            500: {"error_code": "ERRORS_UNEXPECTED_ERROR_ON_ADD_TEAM_MEMBER"},
            403: {"detail": "You do not have permission to perform this action."},
        },
        description="Invites members to a team",
        examples=[
            OpenApiExample(
                "Add member successfully",
                summary="Members added successfully",
                value={"result": "success"},
            ),
            OpenApiExample(
                "Request with domain error",
                summary="Team invitations failed",
                value={"error_code": "ERRORS_ADD_TEAM_MEMBERS_FAILED"},
            ),
            OpenApiExample(
                "Request with low permissions",
                summary="Unauthorized to invite members",
                value={"detail": "You do not have permission to perform this action."},
            ),
        ],
    )
    @action(detail=False, url_path="(?P<team_id>[^/.]+)/members/add", methods=["post"])
    def add_members(self, request, *args, **kwargs) -> Response:
        """
        Given a `team_id`, an `organization_id`, and an array of users, invites
        them to the team.
        """
        serializer = AddTeamMemberRequestSerializer(data={**request.data, **kwargs})
        serializer.is_valid(raise_exception=True)

        try:
            # Invite members
            organizations.add_members(
                user=request.user,
                organization_id=serializer.validated_data["organization_id"],
                team_id=serializer.validated_data["team_id"],
                new_users=serializer.validated_data["users"],
            )

            # Create final response using AddTeamMemberResponseSerializer
            serialized_team_data = AddTeamMemberResponseSerializer(
                {"result": "success"}
            ).data

            return Response(serialized_team_data, status=200)

        except (
            OrganizationAddTeamMemberException,
            OrganizationSingleTeamRetrievalFailedException,
        ):
            return Response(
                ErrorSerializer("ERRORS_ADD_TEAM_MEMBERS_FAILED").data,
                status=400,
            )

        except Exception as e:
            logger.error("Raised unexpected exception on team creation Exception=%s", e)
            return Response(
                ErrorSerializer("ERRORS_UNEXPECTED_ERROR_ON_ADD_TEAM_MEMBER").data,
                status=500,
            )

    @extend_schema(
        parameters=[
            OpenApiParameter(
                name="team_id",
                description="The team id",
                location=OpenApiParameter.PATH,
                type=str,
                required=True,
                examples=[
                    OpenApiExample("Team id example", value="53"),
                ],
            ),
        ],
        request=RemoveTeamMemberRequestSerializer,
        responses={
            200: RemoveTeamMemberResponseSerializer,
            400: {"error_code": "ERRORS_REMOVE_TEAM_MEMBER_FAILED"},
            500: {"error_code": "ERRORS_UNEXPECTED_ERROR_ON_REMOVE_TEAM_MEMBER"},
            403: {"detail": "You do not have permission to perform this action."},
        },
        description="Removes a member from a team",
        examples=[
            OpenApiExample(
                "Successful request",
                summary="Member removed successfully",
                value={"result": "removed"},
            ),
            OpenApiExample(
                "Request with domain error",
                summary="Team member removal failed",
                value={"error_code": "ERRORS_REMOVE_TEAM_MEMBER_FAILED"},
            ),
            OpenApiExample(
                "Request with low permissions",
                summary="Unauthorized to remove member",
                value={"detail": "You do not have permission to perform this action."},
            ),
        ],
    )
    @action(
        detail=False, url_path="(?P<team_id>[^/.]+)/members/remove", methods=["post"]
    )
    def remove_member(self, request, *args, **kwargs) -> Response:
        """
        Given a `team_id` and a user id, checks caller privileges to then remove
        the user from the team.
        """
        serializer = RemoveTeamMemberRequestSerializer(data={**request.data, **kwargs})
        serializer.is_valid(raise_exception=True)

        try:
            # Remove member
            organizations.remove_member(
                user=request.user,
                team_id=serializer.validated_data["team_id"],
                user_id=serializer.validated_data["user_id"],
            )

            # Create final response using AddTeamMemberResponseSerializer
            serialized_team_data = RemoveTeamMemberResponseSerializer(
                {"result": "removed"}
            ).data

            return Response(serialized_team_data, status=200)

        except OrganizationRemoveTeamMemberException:
            return Response(
                ErrorSerializer("ERRORS_REMOVE_TEAM_MEMBER_FAILED").data,
                status=400,
            )

        except Exception as e:
            logger.error(
                "Raised unexpected exception on member removal Exception=%s", e
            )
            return Response(
                ErrorSerializer("ERRORS_UNEXPECTED_ERROR_ON_REMOVE_TEAM_MEMBER").data,
                status=500,
            )

    @extend_schema(
        parameters=[
            OpenApiParameter(
                name="team_id",
                description="The team id",
                location=OpenApiParameter.PATH,
                type=str,
                required=True,
                examples=[
                    OpenApiExample("Team id example", value="53"),
                ],
            ),
        ],
        request=LeaveTeamRequestSerializer,
        responses={
            200: LeaveTeamResponseSerializer,
            400: {"error_code": "ERRORS_LEAVE_TEAM_FAILED"},
            500: {"error_code": "ERRORS_UNEXPECTED_ERROR_ON_LEAVE_TEAM"},
            403: {"detail": "You do not have permission to perform this action."},
        },
        description="Leaves a team",
        examples=[
            OpenApiExample(
                "Successful request",
                summary="Successfully left a team",
                value={"result": "success"},
            ),
            OpenApiExample(
                "Request with domain error",
                summary="Leave team request failed",
                value={"error_code": "ERRORS_REMOVE_TEAM_MEMBER_FAILED"},
            ),
            OpenApiExample(
                "Request with low permissions",
                summary="Unauthorized to leave a team",
                value={"detail": "You do not have permission to perform this action."},
            ),
        ],
    )
    @action(
        detail=False, url_path="(?P<team_id>[^/.]+)/members/leave", methods=["post"]
    )
    def leave_team(self, request, *args, **kwargs) -> Response:
        """
        Given a `team_id` checks caller privileges to then leave the user from
        the team.
        """
        serializer = LeaveTeamRequestSerializer(data={**request.data, **kwargs})
        serializer.is_valid(raise_exception=True)

        try:
            # Remove member
            organizations.leave_team(
                user=request.user, team_id=serializer.validated_data["team_id"]
            )

            # Create final response using AddTeamMemberResponseSerializer
            serialized_team_data = LeaveTeamResponseSerializer(
                {"result": "success"}
            ).data

            return Response(serialized_team_data, status=200)

        except OrganizationLeaveTeamFailedException as e:
            logger.error("An error occurred while trying to leave the team e=%s", e)
            return Response(
                ErrorSerializer(e.error_code).data,
                status=400,
            )

        except Exception as e:
            logger.error(
                "Raised unexpected exception while leaving the team. Exception=%s", e
            )
            return Response(
                ErrorSerializer("ERRORS_UNEXPECTED_ERROR_ON_LEAVE_TEAM").data,
                status=500,
            )

    @extend_schema(
        parameters=[
            OpenApiParameter(
                name="team_id",
                description="The team id",
                location=OpenApiParameter.PATH,
                type=str,
                required=True,
                examples=[
                    OpenApiExample("Team id example", value="53"),
                ],
            ),
        ],
        request=TeamPartialUpdateSerializer,
        responses={
            200: TeamSerializer,
            403: {"detail": "You do not have permission to perform this action."},
        },
    )
    def partial_update(self, request, *args, **kwargs):
        """
        Given a `team_id` and some team attributes defined by the
        `TeamPartialUpdateSerializer`, updates the given team to return a
        team representation.
        """
        # Use the default update logic to handle the partial update
        super().partial_update(request, *args, **kwargs)

        # Retrieve the updated instance
        instance = self.get_object()

        # Serialize the instance using the custom ResponseSerializer
        serializer = TeamSerializer(instance)

        # Return a new response with the serialized data
        return Response(serializer.data)

    @extend_schema(
        responses={
            200: ListTeamGroupsResponseSerializer,
            403: {"detail": "You do not have permission to perform this action."},
        },
        description="Lists team groups",
        examples=[
            OpenApiExample(
                "Successful request",
                summary="Successfully left a team",
                value={
                    "groups": [
                        {"name": "TeamAdmin", "id": 7},
                        {"name": "TeamCollaborator", "id": 8},
                    ]
                },
            ),
            OpenApiExample(
                "Request with low permissions",
                summary="Unauthorized to leave a team",
                value={"detail": "You do not have permission to perform this action."},
            ),
        ],
    )
    @action(detail=False, url_path="groups", methods=["get"])
    def list_permission_groups(self, request, *args, **kwargs) -> Response:
        """
        Given a `team_id` checks caller privileges to then leave the user from
        the team.
        """

        try:
            # Remove member
            groups = organizations.get_team_groups()
            serialized_team_data = ListTeamGroupsResponseSerializer(
                {"groups": groups}
            ).data

            return Response(serialized_team_data, status=200)

        except OrganizationLeaveTeamFailedException as e:
            logger.error("An error occurred while trying to leave the team e=%s", e)
            return Response(
                ErrorSerializer(e.error_code).data,
                status=400,
            )

        except Exception as e:
            logger.error(
                "Raised unexpected exception while retrieving team groups. Exception=%s",
                e,
            )
            return Response(
                ErrorSerializer("ERRORS_UNEXPECTED_ERROR_OBTAINING_TEAM_GROUPS").data,
                status=500,
            )

    @extend_schema(
        request=ListTeamsByUserRequestSerializer,
        responses={
            200: ListTeamsByUserResponseSerializer,
            403: {"detail": "You do not have permission to perform this action."},
        },
        description="List teams by user",
        examples=[
            OpenApiExample(
                "Successful request",
                summary="Successfully list member teams",
                value={
                    "user_team_groups": [
                        {
                            "team": {
                                "id": 12,
                                "name": "Some team name",
                                "description": "some team description",
                                "nc_group_id": "organization-2-general",
                                "nc_conversation_token": "tpodghyu",
                                "organization_id": 2,
                                "organization_name": "Some Organization",
                                "is_personal_workspace": False,
                                "logo": "",
                            },
                            "group": {"id": 7, "name": "TeamAdmin"},
                        },
                        {
                            "team": {
                                "id": 21,
                                "name": "Some other team name",
                                "description": "Some other team description",
                                "nc_group_id": "organization-2-8097f09b-7598-4080-af10-4b02a2707188",
                                "nc_conversation_token": "be3rax66",
                                "organization_id": 2,
                                "organization_name": "Some Organization",
                                "is_personal_workspace": False,
                                "logo": "",
                            },
                            "group": {"id": 8, "name": "TeamCollaborator"},
                        },
                    ]
                },
            ),
            OpenApiExample(
                "Request with low permissions",
                summary="Unauthorized to list teams",
                value={"detail": "You do not have permission to perform this action."},
            ),
        ],
    )
    @action(detail=False, url_path="by_member", methods=["get"])
    def get_teams_by_member(self, request, *_args, **_kwargs) -> Response:
        """
        Given a user that makes a request returns a list of teams for the given
        user.
        """
        try:
            data = {"member_id": request.user.id}
            serializer = ListTeamsByUserRequestSerializer(data=data)
            serializer.is_valid(raise_exception=True)

            # List member teams
            user_team_groups: list[UserTeam] = organizations.get_teams_by_user(
                member_id=serializer.validated_data["member_id"]
            )

            serialized_team_data = ListTeamsByUserResponseSerializer(
                {"user_team_groups": user_team_groups}
            ).data

            return Response(data=serialized_team_data, status=200)

        except Exception as e:
            logger.error(
                "Raised unexpected exception while retrieving team groups. Exception=%s",
                e,
            )
            return Response(
                ErrorSerializer("ERRORS_UNEXPECTED_ERROR_OBTAINING_TEAM_GROUPS").data,
                status=400,
            )

    @extend_schema(
        operation_id="team_members_retrieve",
        parameters=[
            OpenApiParameter(
                name="team_id",
                description="The team id",
                location=OpenApiParameter.PATH,
                type=str,
                required=True,
                examples=[
                    OpenApiExample("Team id example", value="53"),
                ],
            ),
            OpenApiParameter(
                name="member_id",
                description="The member id",
                location=OpenApiParameter.PATH,
                type=str,
                required=True,
                examples=[
                    OpenApiExample("Member id example", value="53"),
                ],
            ),
        ],
        request=GetTeamMemberRequestSerializer,
        responses={
            200: TeamMemberSerializer,
            400: {"error_code": "ERRORS_GET_TEAM_MEMBER_FAILED"},
            500: {"error_code": "ERRORS_UNEXPECTED_ERROR_ON_GET_TEAM_MEMBER"},
            403: {"detail": "You do not have permission to perform this action."},
        },
        description="Retrieves a member of a team",
        examples=[
            OpenApiExample(
                "Retrieve member successfully",
                summary="Member is retrieved successfully",
                value={
                    "user": {
                        "id": 13,
                        "username": "johnny",
                        "full_name": "John paul",
                        "email": "john@company.com",
                    },
                    "group": {"id": 6, "name": "TeamOwner"},
                },
            ),
            OpenApiExample(
                "Request with domain error",
                summary="Team member retrieval failed",
                value={"error_code": "ERRORS_GET_TEAM_MEMBER_FAILED"},
            ),
            OpenApiExample(
                "Request with low permissions",
                summary="Unauthorized to list team members",
                value={"detail": "You do not have permission to perform this action."},
            ),
        ],
    )
    @action(
        detail=False,
        url_path="(?P<team_id>[^/.]+)/members/(?P<member_id>[^/.]+)/show",
        methods=["get"],
        url_name="get_team_member",
    )
    def get_member(self, request, *_args, **kwargs) -> Response:
        """
        Given a `team_id` and a `member_id` returns a team member details.
        """
        serializer = GetTeamMemberRequestSerializer(data={**request.data, **kwargs})
        serializer.is_valid(raise_exception=True)

        try:
            # Get member
            team_member = organizations.get_team_member(
                team_id=serializer.validated_data["team_id"],
                member_id=serializer.validated_data["member_id"],
            )
            # Serialize response
            serialized_team_data = TeamMemberSerializer(
                {"user": team_member.get("user"), "group": team_member.get("group")}
            ).data

            return Response(serialized_team_data, status=200)

        except GetTeamMemberFailedException:
            return Response(
                ErrorSerializer("ERRORS_GET_TEAM_MEMBER_FAILED").data,
                status=400,
            )

        except Exception as e:
            logger.error(
                "Raised unexpected exception on team member retrieval. Exception=%s", e
            )
            return Response(
                ErrorSerializer("ERRORS_UNEXPECTED_ERROR_ON_LIST_TEAM_MEMBERS").data,
                status=500,
            )

    @extend_schema(
        parameters=[
            OpenApiParameter(
                name="team_id",
                description="The team id",
                location=OpenApiParameter.PATH,
                type=str,
                required=True,
                examples=[
                    OpenApiExample("Team id example", value="53"),
                ],
            ),
            OpenApiParameter(
                name="user_id",
                description="The user id",
                location=OpenApiParameter.PATH,
                type=str,
                required=True,
                examples=[
                    OpenApiExample("User id example", value="27"),
                ],
            ),
        ],
        request=UpdateUserTeamPermissionsRequestSerializer,
        responses={
            200: UpdateUserTeamPermissionsResponseSerializer,
            400: {"error_code": "ERRORS_LEAVE_TEAM_FAILED"},
            500: {"error_code": "ERRORS_UNEXPECTED_ERROR_ON_LEAVE_TEAM"},
            403: {"detail": "You do not have permission to perform this action."},
        },
        description="Update team permissions for a user.",
        examples=[
            OpenApiExample(
                "Successful request",
                summary="Successfully updated team permissions",
                value={"result": "success", "group": {"name": "TeamAdmin", "id": 7}},
            ),
            OpenApiExample(
                "Request with domain error",
                summary="Leave team request failed",
                value={"error_code": "ERRORS_UPDATE_USER_TEAM_PERMISSIONS_FAILED"},
            ),
            OpenApiExample(
                "Request with low privileges",
                summary="Unauthorized to leave a team",
                value={"detail": "You do not have permission to perform this action."},
            ),
        ],
    )
    @action(
        detail=False,
        url_path="(?P<team_id>[^/.]+)/members/(?P<user_id>[^/.]+)/permissions",
        methods=["post"],
    )
    def update_team_permissions(self, request, *args, **kwargs) -> Response:
        """
        Given a `team_id`, a `user_id` and a `group_id`, promotes or demotes
        user privileges.
        """
        serializer = UpdateUserTeamPermissionsRequestSerializer(
            data={**request.data, **kwargs}
        )
        serializer.is_valid(raise_exception=True)

        try:
            # Update team permissions
            user_team = organizations.update_user_team_permissions(
                user=request.user,
                team_id=serializer.validated_data["team_id"],
                user_id=serializer.validated_data["user_id"],
                group_id=serializer.validated_data["group_id"],
            )

            serialized_team_data = UpdateUserTeamPermissionsResponseSerializer(
                {"result": "success", "group": user_team.group}
            ).data

            return Response(serialized_team_data, status=200)

        except OrganizationUpdateUserTeamPermissionsFailedException as e:
            logger.error(
                "An error occurred while trying to update user permissions for a team e=%s",
                e,
            )
            return Response(
                ErrorSerializer(e.error_code).data,
                status=400,
            )

        except Exception as e:
            logger.error(
                "Raised unexpected exception while trying to update user team permissions. Exception=%s",
                e,
            )
            return Response(
                ErrorSerializer("ERRORS_UNEXPECTED_ERROR_ON_PERMISSIONS_UPDATE").data,
                status=500,
            )


class ShareUploadAPIView(APIView):
    parser_classes = [MultiPartParser]
    permission_classes = [permissions.IsAuthenticated]
    authentication_classes = [JWTAuthentication]
    serializer_class = ShareUploadRequestSerializer

    @extend_schema(
        request=ShareUploadRequestSerializer,
        parameters=[
            OpenApiParameter(
                name="filename",
                description="A name for the file",
                location=OpenApiParameter.PATH,
                type=str,
                required=True,
                examples=[
                    OpenApiExample(
                        "File with name hello-world.jpg", value="hello-world.jpg"
                    ),
                    OpenApiExample("File with name free.pdf", value="free.pdf"),
                ],
            ),
        ],
    )
    def post(self, request, **kwargs):
        file_names_list = nextcloud_files_utils.list_file_names(request.user)
        data = {
            "filename": create_file_name(request.data.get("filename"), file_names_list),
            "file": request.data.get("file"),
            "teams": request.data.get("teams"),
        }
        request_serializer = self.serializer_class(
            data=data, context={"user_id": request.user.id}
        )
        if not request_serializer.is_valid():
            return Response(
                ErrorSerializer(
                    "ERRORS_SHARE_FILE_IN_TEAMS_INVALID_REQUEST_PARAMS"
                ).data,
                status=400,
            )

        args = list(
            {**request_serializer.validated_data, "user": request.user}.values()
        )
        try:
            organization_share_manager.upload(*args)
            return Response(None, status=200)

        except Exception:
            return Response(
                ErrorSerializer("ERRORS_SHARE_UPLOAD_FAILED").data, status=400
            )


class ProjectExportAPIView(APIView):
    parser_classes = [MultiPartParser]
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = ShareProjectRequestSerializer

    @extend_schema(
        request=ShareProjectRequestSerializer,
        parameters=[
            OpenApiParameter(
                name="filename",
                description="A name for the project",
                location=OpenApiParameter.PATH,
                type=str,
                required=True,
                examples=[
                    OpenApiExample(
                        "Project with name audio_example.zip", value="audio_example.zip"
                    ),
                    OpenApiExample(
                        "Project with name project_example.zip",
                        value="project_example.zip",
                    ),
                ],
            ),
        ],
        operation_id="export_project",
    )
    def post(self, request, **kwargs):
        project_names_list = nextcloud_files_utils.list_project_names(request.user)
        data = {
            "filename": create_file_name(
                request.data.get("filename"), project_names_list
            ),
            "file": request.data.get("file"),
            "teams": request.data.get("teams"),
        }
        request_serializer = self.serializer_class(
            data=data, context={"user_id": request.user.id}
        )

        if not request_serializer.is_valid():
            return Response(
                ErrorSerializer("ERRORS_SHARE_PROJECT_INVALID_REQUEST_PARAMS").data,
                status=400,
            )

        args = list(
            {**request_serializer.validated_data, "user": request.user}.values()
        )
        try:
            organization_share_manager.upload_project(*args)
            return Response("PROJECT_EXPORTED_SUCCESSFULLY", status=200)

        except OrganizationShareUploadedFileException as e:
            return Response(e.message, status=400)
        except Exception:
            return Response(
                ErrorSerializer("ERRORS_SHARE_PROJECT_FAILED").data, status=400
            )


class ProjectListAPIView(APIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = ProjectRequestSerializer

    @extend_schema(
        request=ProjectRequestSerializer,
        responses=ProjectResponseSerializer(many=True),
        description="Returns all projects for the given user",
        examples=[
            OpenApiExample(
                "List of projects",
                summary="Returns a list of projects",
                value=[
                    {
                        "id": 1333,
                        "name": "2024-04-08-10-06-26-project.zip",
                        "shared_teams": [{"team_id": 1, "name": "ExampleTeam"}],
                    },
                ],
            )
        ],
        operation_id="list_projects",
    )
    def get(self, request, *args, **kwargs):
        try:
            response = organization_share_manager.list_projects(request.user)
            return Response(response, status=200)
        except OrganizationProjectsRetrievalException as e:
            return Response(
                ErrorSerializer(e.message).data,
                status=400,
            )


class ProjectImportAPIView(APIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = ProjectImportRequestSerializer

    @extend_schema(
        request=ProjectImportRequestSerializer,
        parameters=[
            OpenApiParameter(
                name="project_id",
                description="A project id",
                location=OpenApiParameter.PATH,
                type=int,
                required=True,
                examples=[
                    OpenApiExample("File with ID 444", value=444),
                ],
            ),
        ],
        operation_id="import_project",
        responses={
            (200, "application/octet-stream"): OpenApiTypes.BINARY,
            (404, "application/json"): {"error_code": "ERRORS_PROJECT_NOT_FOUND"},
        },
    )
    def get(self, request, **kwargs):
        """
        Given a project id returns a file from Nextcloud, representing a
        project.
        """
        data = {
            "project_id": kwargs.get("project_id"),
        }
        request_serializer = ProjectImportRequestSerializer(data=data)
        if not request_serializer.is_valid():
            # TODO: Find a way to send the serializer error dictionary, in addition to the error message.
            return Response(
                ErrorSerializer("ERRORS_INVALID_IMPORT_PROJECT_REQUEST_PARAMS").data,
                status=400,
            )

        try:
            file_bytestring = organizations.download_project(
                request_serializer.data["project_id"], request.user
            )
            encoded_file = encoding.encode_file_bytestring(file_bytestring)
            return HttpResponse(
                encoded_file, content_type="application/octet-stream", status=200
            )
        except OrganizationProjectNotFoundException:
            return Response(
                ErrorSerializer("ERRORS_PROJECT_NOT_FOUND").data, status=404
            )

        except OrganizationProjectDownloadException:
            return Response(
                ErrorSerializer(
                    "ERRORS_UNEXPECTED_ERROR_WHILE_DOWNLOADING_PROJECT"
                ).data,
                status=400,
            )


class OrganizationMembersView(APIView):
    """
    The OrganizationMembersView defines the operation to get the members of an organization.
    """

    serializer_class = OrganizationMembersRequestSerializer
    permission_classes = [IsAuthenticated, IsAdminOrOrgOwner]

    @extend_schema(
        operation_id="get_organization_members",
        request=OrganizationMembersRequestSerializer,
        responses={
            200: OrganizationMembersResponseSerializer,
            401: {"error_code": "ERRORS_UNAUTHORIZED_USER"},
        },
        parameters=[
            OpenApiParameter(
                name="organization_id",
                description="The organization Id",
                location=OpenApiParameter.PATH,
                type=int,
                required=True,
            ),
        ],
    )
    def get(self, request, **kwargs):
        data = {"organization_id": kwargs.get("organization_id")}
        serializer = self.serializer_class(data=data)

        try:
            serializer.is_valid(raise_exception=True)
            active_members, suspended_members = organizations.get_organization_members(
                serializer.data.get("organization_id"), request.user
            )

            members_data = {
                "active": {"users": active_members},
                "suspended": {"users": suspended_members},
            }

            return Response(
                data=OrganizationMembersResponseSerializer(members_data).data,
                status=200,
            )
        except UnauthorizedUserException:
            return Response(
                data=ErrorSerializer("ERRORS_UNAUTHORIZED_USER").data, status=401
            )


class ExternalHyperlinksViewSet(viewsets.ModelViewSet):
    queryset = ExternalHyperlink.objects.all()
    serializer_class = ExternalHyperlinkSerializer

    def get_permissions(self):
        base_permissions = [IsAuthenticated(), BelongsToOrganization()]
        if self.action in ["create", "update", "partial_update", "destroy"]:
            base_permissions.append(IsAdminOrOrgOwner())
        return base_permissions

    def get_queryset(self):
        user = self.request.user
        try:
            organization = UserOrganizationGroup.objects.get(user=user).organization
        except UserOrganizationGroup.DoesNotExist:
            raise NotFound("Organization not found for the user.")

        return ExternalHyperlink.objects.filter(organization_id=organization.id)

    @extend_schema(
        request=ExternalHyperlinkCreateSerializer,
        parameters=[
            OpenApiParameter(
                name="organization_id",
                description="User organization's ID",
                type=int,
                required=True,
            ),
        ],
        description="Creates a new ExternalHyperlink for the user's organization",
        responses={
            201: ExternalHyperlinkSerializer,
            400: {"error": "Organization not found"},
            403: {"detail": "You do not have permission to perform this action."},
        },
    )
    def create(self, request, *args, **kwargs):
        organization_id = kwargs.get("organization_id")
        if not organization_id:
            return Response({"error": "Organization ID is required"}, status=400)

        try:
            organization = Organization.objects.get(id=organization_id)
        except Organization.DoesNotExist:
            return Response({"error": "Organization not found"}, status=400)

        data = {**request.data, "organization_id": organization.id}
        serializer = ExternalHyperlinkCreateSerializer(data=data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(serializer.data, status=201)


class FavouriteTeamViewSet(
    mixins.CreateModelMixin,
    mixins.DestroyModelMixin,
    mixins.ListModelMixin,
    viewsets.GenericViewSet,
):
    queryset = FavouriteTeam.objects.all()

    def get_serializer_class(self):
        """Use different serializers for different actions."""
        if self.action in ["list"]:
            return FavouriteTeamResponseSerializer
        return FavouriteTeamSerializer

    def get_queryset(self):
        """Filter favourites by the current user."""
        return FavouriteTeam.objects.filter(user=self.request.user)

    @extend_schema(
        request=FavouriteTeamCreateSerializer,
        description="Creates a new FavouriteTeam for the user",
        responses={
            201: FavouriteTeamSerializer,
            400: {"error": "ERRORS_USER_DOES_NOT_BELONG_TO_TEAM"},
        },
    )
    def create(self, request, *args, **kwargs):
        """
        Given a `team_id`, marks the team as favourite for the current user.
        """

        team = request.data.get("team_id")
        try:
            max_teams = config.MAX_FAVOURITE_TEAMS
            UserTeam.objects.get(user=request.user, team_id=team)

            favourites = FavouriteTeam.objects.filter(user=request.user).count()

            if favourites == max_teams:
                return Response(
                    {
                        "error": "ERRORS_REACHED_MAX_FAVOURITE_TEAMS_COUNT",
                        "count": max_teams,
                    },
                    status=400,
                )

        except UserTeam.DoesNotExist:
            return Response(
                {"error": "ERRORS_USER_DOES_NOT_BELONG_TO_TEAM"}, status=400
            )

        serializer = self.get_serializer(data={"team": team, "user": request.user.id})
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=201)

    def destroy(self, request, *args, **kwargs):
        """
        Given a `team_id`, deletes the favourite team for the current user.
        """

        instance = self.get_object()

        if instance.user != request.user:
            return Response(
                {"error": "ERRORS_UNAUTHORIZED_TO_DELETE_FAVOURITE_TEAM"},
                status=403,
            )

        return super().destroy(request, *args, **kwargs)
