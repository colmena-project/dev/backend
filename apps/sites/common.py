from colmena.utils import encoding


def build_site_with_token(url_path, args, **kwargs):
    uidb64 = encoding.base64_encode_int(args["pk"])
    token = args["token"]
    site_url = (
        url_path.replace("<pk>", uidb64)
        .replace("<uid>", uidb64)
        .replace("<token>", token)
    )
    return site_url
