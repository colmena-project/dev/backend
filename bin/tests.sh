#!/bin/sh
set -e

bin=$PYTHON

CYAN='\033[0;36m'
GREEN='\033[0;32m'
BLUE='\033[34m'
NOFORMAT='\033[0m'
PURPLE='\033[0;35m'
RED='\033[0;31m' 
YELLOW='\033[0;33m'

watch_all() {
  set -e

  echo ""
  echo "${GREEN}Watching all tests${NOFORMAT}"
  echo ""

  local command="$bin manage.py test --verbosity=0 --parallel --settings=colmena.settings.test"

  $command || true

  while true; do
    inotifywait -r -e modify,create,delete .
    
    $command || true
  done
}

watch_tag() {
  set -e
  local tag=$1
  echo ""
  echo "${GREEN}Watching tests with TAG: '${CYAN}$tag${GREEN}'${NOFORMAT}"
  echo ""
  echo "Import the tag utility and use the tag decorator on your tests:"
  echo ""
  echo "${PURPLE}from${NOFORMAT} django.test ${PURPLE}import${NOFORMAT} tag"
  echo ""
  echo "Example:"
  echo ""
  echo "${CYAN}@tag${YELLOW}(${GREEN}\"$tag\"${YELLOW})${NOFORMAT}"
  echo "${PURPLE}def ${CYAN}my_awesome_test${YELLOW}(${BLUE}self${YELLOW})${NOFORMAT}:"
  echo "  ${RED}self${NOFORMAT}.assertTrue${YELLOW}(${BLUE}True${YELLOW})${NOFORMAT}"
  echo ""

  local command="$bin manage.py test --verbosity=0 --parallel --tag=wip --settings=colmena.settings.test"

  $command || true

  while true; do
    inotifywait -r -e modify,create,delete .
    
    $command || true
  done
}

case $1 in
  watch_all) "$@"; exit;;
  watch_tag) "$@"; exit;;
esac
