from django.utils.translation import gettext_lazy

from constance.apps import ConstanceConfig


class Constance(ConstanceConfig):
    verbose_name = gettext_lazy("constance_app_name")
