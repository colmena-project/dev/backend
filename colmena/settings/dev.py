from .base import *

# ------------------------------------------------------------------------------
# Application Settings
#
DEBUG = True
DEBUG_TOOLS = os.environ.get("DEBUG_TOOLS", "0") == "1"

# ------------------------------------------------------------------------------
# Logger Settings
#

LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "handlers": {
        "console": {
            "level": "DEBUG",
            "class": "logging.StreamHandler",
            "formatter": "verbose-colored",
        },
        "file": {
            "level": "ERROR",
            "class": "logging.FileHandler",
            "filename": "logs/dev/error.log",
            "formatter": "verbose",
        },
    },
    "root": {
        "handlers": ["console"],
        "level": "DEBUG",
    },
    "loggers": {
        "django": {
            "handlers": ["console"],
            "level": "INFO",
            "propagate": False,
        },
        "colmena": {
            "handlers": ["console", "file"],
            "level": os.getenv("DJANGO_LOG_LEVEL", "DEBUG"),
            "propagate": False,
        },
        "apps": {
            "handlers": ["console", "file"],
            "level": os.getenv("DJANGO_LOG_LEVEL", "DEBUG"),
            "propagate": False,
        },
    },
    "formatters": {
        "verbose": {
            "format": "{asctime} {levelname} {name} module={module} process={process:d} thread={thread:d} msg='{message}'",
            "style": "{",
        },
        "verbose-colored": {
            "()": "colorlog.ColoredFormatter",
            "format": "%(log_color)s%(asctime)-s %(levelname)-s {%(name)s} [%(module)s] %(message)s",
            "style": "%",
            "log_colors": {
                "DEBUG": "cyan",
                "INFO": "white",
                "WARNING": "yellow",
                "ERROR": "red",
                "CRITICAL": "bold_red",
            },
        },
    },
}

# ------------------------------------------------------------------------------
# Security Settings
#
SECRET_KEY = "colmena-dev-secret-key"
CORS_ORIGIN_ALLOW_ALL = True
CSRF_COOKIE_SECURE = False
SESSION_COOKIE_SECURE = False

INTERNAL_IPS = ["127.0.0.1"]

# ------------------------------------------------------------------------------
# Apps
#

DEV_ONLY_APPS = []
DEV_ONLY_APPS = ["django_extensions"]

if DEBUG_TOOLS:
    DEV_ONLY_APPS.append("debug_toolbar")

INSTALLED_APPS = INSTALLED_APPS + DEV_ONLY_APPS

#
# Middleware
#
DEV_ONLY_MIDDLEWARE = []
if DEBUG_TOOLS:
    DEV_ONLY_MIDDLEWARE = ["debug_toolbar.middleware.DebugToolbarMiddleware"]

MIDDLEWARE = MIDDLEWARE + DEV_ONLY_MIDDLEWARE

# ------------------------------------------------------------------------------
# Database Settings
#
DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": os.environ.get("POSTGRES_DATABASE", "colmena_dev"),
        "USER": os.environ.get("POSTGRES_USERNAME", "postgres"),
        "PASSWORD": os.environ.get("POSTGRES_PASSWORD", "postgres"),
        "HOST": os.environ.get("POSTGRES_HOSTNAME", "localhost"),
        "PORT": os.environ.get("POSTGRES_PORT", "5432"),
    }
}

# ------------------------------------------------------------------------------
# Email Settings
#
EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"
EMAIL_HOST = "localhost"
EMAIL_PORT = 1025
EMAIL_HOST_USER = ""
EMAIL_HOST_PASSWORD = ""
EMAIL_USE_SSL = False
EMAIL_TIMEOUT = 5
