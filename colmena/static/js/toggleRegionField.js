document.addEventListener('DOMContentLoaded', function () {
    const groupsSelect = document.getElementById('id_groups');
    const regionField = document.getElementById('id_region');

    function toggleRegionField() {
        const selectedValue = groupsSelect.options[groupsSelect.selectedIndex].text;
        const regionFormRow = regionField.closest('.form-row');

        if (selectedValue === 'Superadmin') {
            regionFormRow.style.display = 'none';
            regionField.value = '';
        } else {
            regionFormRow.style.display = '';
        }
    }

    toggleRegionField();
    groupsSelect.addEventListener('change', toggleRegionField);
});