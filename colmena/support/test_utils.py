import random, string


def unique_value(value):
    random_name = "".join(random.choice(string.ascii_letters) for _ in range(10))
    return f"{random_name}{value}"
