import base64
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.utils.http import urlsafe_base64_decode


def base64_encode_int(value: int) -> str:
    """
    Given an integer value, returnes a base64 encoded string.
    """
    return urlsafe_base64_encode(force_bytes(value))


def base64_decode_int(value: str) -> int:
    """
    Given a string value in base64, decodes and returns an integer.
    """
    return int(urlsafe_base64_decode(value))


def encode_file_bytestring(bytestring: bytes | bytearray | memoryview) -> bytes:
    """
    Given a bytestring, returns a base64 representation in bytes.
    """
    return base64.b64encode(bytestring)
