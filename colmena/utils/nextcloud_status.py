import json
from urllib.request import urlopen
from urllib.error import URLError
from apps.nextcloud.config import get_nextcloud_api_url


def get_nextcloud_api_status():
    try:
        with urlopen(f"{get_nextcloud_api_url()}/status.php") as response:
            data = json.loads(response.read())
            data["status"] = "ok"
            keys_to_remove = [
                "needsDbUpgrade",
                "version",
                "productname",
                "extendedSupport",
                "edition",
            ]
            for k in keys_to_remove:
                data.pop(k, None)

            return data
    except URLError:
        return {"status": "error"}
