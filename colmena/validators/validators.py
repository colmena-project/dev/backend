import re

from django.core.validators import RegexValidator
from django.utils.translation import gettext_lazy


class ValidatorOnlyLettersAndSpaces(RegexValidator):
    regex = r"^[a-zA-Zá-üÁ-ÜñÑ\s]+$"
    message = gettext_lazy("The field must contains letters and spaces")


def validate_characters(value: str) -> bool:
    return not re.match(r"^[a-zA-Z0-9 _\.\@\-\']+$", value)


def validate_password_characters(value: str) -> bool:
    return not "ñ" in value
