# syntax=docker/dockerfile:1
FROM python:3.10-slim-buster
ENV PYTHONUNBUFFERED 1
RUN apt-get update && apt-get install -y --no-install-recommends \
  git \
  build-essential \
  libsasl2-dev \
  gettext \
  nginx \
  curl

WORKDIR "/opt/app"

COPY requirements/base.txt /opt/app/requirements/base.txt
COPY requirements/prod.txt /opt/app/requirements/prod.txt

RUN pip install -U pip
RUN pip install -r requirements/prod.txt

COPY . /opt/app

RUN python -m openapi_python_generator \
  apps/nextcloud/openapi/schema.json \
  apps/nextcloud/openapi/client

COPY devops/local/nginx/app /etc/nginx/sites-available/app

RUN ln -s /etc/nginx/sites-available/app /etc/nginx/sites-enabled/
RUN nginx -t

COPY devops/builder/entrypoint.sh .

RUN chmod +x entrypoint.sh
RUN chown nobody /opt/app

CMD /opt/app/entrypoint.sh start_local
